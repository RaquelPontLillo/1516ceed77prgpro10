package org.ceedcv.ceed77prgpro10.controlador;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import org.ceedcv.ceed77prgpro10.modelo.IModelo;
import org.ceedcv.ceed77prgpro10.modelo.ModeloDb4o;
import org.ceedcv.ceed77prgpro10.modelo.ModeloFichero;
import org.ceedcv.ceed77prgpro10.modelo.ModeloHibernate;
import org.ceedcv.ceed77prgpro10.modelo.ModeloMysql;
import org.ceedcv.ceed77prgpro10.vista.Funciones;
import org.ceedcv.ceed77prgpro10.vista.VistaCursoAlumno;
import org.ceedcv.ceed77prgpro10.vista.VistaGrafica;
import org.ceedcv.ceed77prgpro10.vista.VistaGraficaAcerca;
import org.ceedcv.ceed77prgpro10.vista.VistaGraficaAlumno;
import org.ceedcv.ceed77prgpro10.vista.VistaGraficaCurso;
import org.ceedcv.ceed77prgpro10.vista.VistaGraficaMatricula;
import org.ceedcv.ceed77prgpro10.vista.VistaTablaAlumnos;
import org.ceedcv.ceed77prgpro10.vista.VistaTablaCursos;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class Controlador implements ActionListener {

	private IModelo modelo = null;
	private VistaGrafica vista = null;
	private Funciones f = new Funciones();
	private String s;

	public Controlador(IModelo m, VistaGrafica v) throws IOException {
		//Tomamos el modelo
		seleccionarModelo();

		//Asignamos el modelo y la vista
		vista = new VistaGrafica();

		//Centrar JFrame
		vista.setLocationRelativeTo(null);

		//Añadimos los listeners
		vista.getAlumno().addActionListener(this);
		vista.getAluCur().addActionListener(this);
		vista.getTablaAlumnos().addActionListener(this);
		vista.getCurso().addActionListener(this);
		vista.getCurAlu().addActionListener(this);
		vista.getTablaCursos().addActionListener(this);
		vista.getMatricula().addActionListener(this);
		vista.getCrearbbdd().addActionListener(this);
		vista.getCreartablas().addActionListener(this);
		vista.getCreardatos().addActionListener(this);
		vista.getBorrarbbdd().addActionListener(this);
		vista.getDocWeb().addActionListener(this);
		vista.getDocPdf().addActionListener(this);
		vista.getAcerca().addActionListener(this);
		vista.getSalir().addActionListener(this);

		//Seteamos la interfaz según el modelo
		switch (s) {
			case "db4o":
				vista.getCreardatos().setVisible(false);
				vista.getCreartablas().setVisible(false);
				vista.getTablaAlumnos().setVisible(false);
				vista.getTablaCursos().setVisible(false);
				break;
			case "mysql":
				vista.getCreardatos().setVisible(true);
				vista.getCreartablas().setVisible(true);
				vista.getTablaAlumnos().setVisible(false);
				vista.getTablaCursos().setVisible(false);
				break;
			case "csv":
				vista.getCreardatos().setVisible(true);
				vista.getCreartablas().setVisible(false);
				vista.getTablaAlumnos().setVisible(false);
				vista.getTablaCursos().setVisible(false);
				break;
			case "hibernate":
				vista.getCreardatos().setVisible(true);
				vista.getCreartablas().setVisible(true);
				vista.getTablaAlumnos().setVisible(true);
				vista.getTablaCursos().setVisible(true);
				break;
		}
	}

	//Implementamos los métodos de las interfaces de eventos
	@Override
	public void actionPerformed(ActionEvent e) {
		Object objeto = e.getSource();
		if (objeto == vista.getSalir()) {
			salir();
		} else if (objeto == vista.getAlumno()) {
			alumno();
		} else if (objeto == vista.getAluCur()) {
			aluCur();
		} else if (objeto == vista.getTablaAlumnos()) {
			tablaAlumnos();
		} else if (objeto == vista.getCurso()) {
			curso();
		} else if (objeto == vista.getCurAlu()) {
			curAlu();
		} else if (objeto == vista.getTablaCursos()) {
			tablaCursos();
		} else if (objeto == vista.getMatricula()) {
			matricula();
		} else if (objeto == vista.getCrearbbdd()) {
			crearBd();
		} else if (objeto == vista.getCreartablas()) {
			crearTablas();
		} else if (objeto == vista.getCreardatos()) {
			crearDatos();
		} else if (objeto == vista.getBorrarbbdd()) {
			eliminarBd();
		} else if (objeto == vista.getDocPdf()) {
			documentacionPdf();
		} else if (objeto == vista.getDocPdf()) {
			documentacionPdf();
		} else if (objeto == vista.getDocWeb()) {
			documentacionWeb();
		} else if (objeto == vista.getAcerca()) {
			acerca();
		}
	}

	public void mousePressed(MouseEvent e) {
		//No hacer nada
	}

	public void mouseReleased(MouseEvent e) {
		//No hacer nada
	}

	public void mouseClicked(MouseEvent e) {
		//No hacer nada
	}

	private void salir() {
		System.exit(0);
	}

	private void alumno() {
		VistaGraficaAlumno v = VistaGraficaAlumno.getInstancia();
		v.pack();
		if (!v.isVisible()) {
			vista.getEscritorio().add(v);
			v.setVisible(true);
		}
		try {
			v.setMaximum(true);
			v.setSelected(true);
		} catch (PropertyVetoException ex) {
			ex.printStackTrace();
		}
		ControladorAlumno ca = new ControladorAlumno(modelo, v);
	}

	private void aluCur() {
		VistaCursoAlumno v = VistaCursoAlumno.getInstancia();
		v.pack();
		if (!v.isVisible()) {
			vista.getEscritorio().add(v);
			v.setVisible(true);
		}
		try {
			v.setMaximum(true);
			v.setSelected(true);
		} catch (PropertyVetoException ex) {
			ex.printStackTrace();
		}
		ControladorCurAlu cac = new ControladorCurAlu(modelo, vista, v, "alucur");
	}

	private void tablaAlumnos() {
		JInternalFrame jiframe = new JInternalFrame("Formulario maestro de alumnos");
		VistaTablaAlumnos jpanel = new VistaTablaAlumnos();
		jiframe.add(jpanel);
		vista.getEscritorio().add(jiframe);
		jiframe.setVisible(true);
		try {
			jiframe.setMaximum(true);
			jiframe.setSelected(true);
		} catch (PropertyVetoException localPropertyVetoException) {
		}
	}

	private void curso() {
		VistaGraficaCurso v = VistaGraficaCurso.getInstancia();
		v.pack();
		if (!v.isVisible()) {
			vista.getEscritorio().add(v);
			v.setVisible(true);
		}
		try {
			v.setMaximum(true);
			v.setSelected(true);
		} catch (PropertyVetoException ex) {
			ex.printStackTrace();
		}
		ControladorCurso cc = new ControladorCurso(modelo, v);
	}

	private void curAlu() {
		VistaCursoAlumno v = VistaCursoAlumno.getInstancia();
		v.pack();
		if (!v.isVisible()) {
			vista.getEscritorio().add(v);
			v.setVisible(true);
		}
		try {
			v.setMaximum(true);
			v.setSelected(true);
		} catch (PropertyVetoException ex) {
			ex.printStackTrace();
		}
		ControladorCurAlu cac = new ControladorCurAlu(modelo, vista, v, "curalu");
	}

	private void tablaCursos() {
		JInternalFrame jiframe = new JInternalFrame("Formulario maestro de cursos");
		VistaTablaCursos jpanel = new VistaTablaCursos();
		jiframe.add(jpanel);
		vista.getEscritorio().add(jiframe);
		jiframe.setVisible(true);
		try {
			jiframe.setMaximum(true);
			jiframe.setSelected(true);
		} catch (PropertyVetoException localPropertyVetoException) {
		}
	}

	private void matricula() {
		VistaGraficaMatricula v = VistaGraficaMatricula.getInstancia();
		v.pack();
		if (!v.isVisible()) {
			vista.getEscritorio().add(v);
			v.setVisible(true);
		}
		try {
			v.setMaximum(true);
			v.setSelected(true);
		} catch (PropertyVetoException ex) {
			Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
		}
		ControladorMatricula cm = new ControladorMatricula(modelo, v);
	}

	private void documentacionWeb() {
		String url = "https://docs.google.com/document/d/1pTpLNawKELyIV_Vn_pu5DMgM-HdIpmN13lkvVuf6X3o/edit?ts=5624d784";
		Desktop desktop = Desktop.getDesktop();
		if (desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.getDesktop().browse(new URI(url));
			} catch (URISyntaxException | IOException ex) {
				Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	private void documentacionPdf() {
		try {
			String ruta = "src/main/java/org/ceedcv/ceed77prgpro9/documentacion/documentacion.pdf";
			File pdf = new File(ruta);
			Desktop.getDesktop().open(pdf);
		} catch (IOException ex) {
			Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	//Funciones de la base de datos
	private void crearBd() {
		try {
			modelo.crearBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void crearTablas() {
		try {
			modelo.crearTablas();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void crearDatos() {
		try {
			modelo.crearDatos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void eliminarBd() {
		try {
			modelo.eliminarBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void acerca() {
		VistaGraficaAcerca v = VistaGraficaAcerca.getInstance();
		v.pack();
		if (!v.isVisible()) {
			vista.getEscritorio().add(v);
			v.setVisible(true);
		}
		try {
			v.setMaximum(true);
			v.setSelected(true);
		} catch (PropertyVetoException ex) {
			Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/* Otras funciones */

	private void seleccionarModelo() throws IOException {
		String[] modelos = {"CSV", "MYSQL", "DB4O", "HIBERNATE"};
		ImageIcon icon = new ImageIcon(getClass().getResource("/imagenes/pregunta.png"));
		int respuesta = JOptionPane.showOptionDialog(null, "Escoge sistema para guardar los datos", "Escoger sistema",
				0, 0, icon, modelos, null);
		switch (respuesta) {
			case 0:
				modelo = new ModeloFichero();
				s = "csv";
				break;
			case 1:
				modelo = new ModeloMysql();
				s = "mysql";
				break;
			case 2:
				modelo = new ModeloDb4o();
				s = "db4o";
				break;
			case 3:
				modelo = new ModeloHibernate();
				s = "hibernate";
		}
	}
}
