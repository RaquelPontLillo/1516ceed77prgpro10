package org.ceedcv.ceed77prgpro10.controlador;

import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.ceedcv.ceed77prgpro10.modelo.Alumnos;
import org.ceedcv.ceed77prgpro10.modelo.Cursos;
import org.ceedcv.ceed77prgpro10.modelo.IModelo;
import org.ceedcv.ceed77prgpro10.modelo.Matriculas;
import org.ceedcv.ceed77prgpro10.vista.Funciones;
import org.ceedcv.ceed77prgpro10.vista.VistaGraficaMatricula;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class ControladorMatricula implements ActionListener, MouseListener, ItemListener {
	private IModelo modelo = null;
	private VistaGraficaMatricula vista;
	private ArrayList alumnos;
	private ArrayList cursos;
	private ArrayList matriculas;
	private String chivato;
	private Matriculas actual;
	private int indice;

	public ControladorMatricula(IModelo m, VistaGraficaMatricula v) {
		modelo = m;
		vista = v;
		cargaCombo();

		//Añadimos los listeners
		vista.getSalir().addActionListener(this);
		vista.getGuardar().addActionListener(this);
		vista.getCancelar().addActionListener(this);
		vista.getCreate().addActionListener(this);
		vista.getUpdate().addActionListener(this);
		vista.getRead().addActionListener(this);
		vista.getDelete().addActionListener(this);
		vista.getSearch().addActionListener(this);
		vista.getPrimero().addActionListener(this);
		vista.getAnterior().addActionListener(this);
		vista.getSiguiente().addActionListener(this);
		vista.getUltimo().addActionListener(this);

		vista.getSalir().addMouseListener(this);
		vista.getGuardar().addMouseListener(this);
		vista.getCancelar().addMouseListener(this);
		vista.getCreate().addMouseListener(this);
		vista.getUpdate().addMouseListener(this);
		vista.getRead().addMouseListener(this);
		vista.getDelete().addMouseListener(this);
		vista.getSearch().addMouseListener(this);
		vista.getPrimero().addMouseListener(this);
		vista.getAnterior().addMouseListener(this);
		vista.getSiguiente().addMouseListener(this);
		vista.getUltimo().addMouseListener(this);

		vista.getCbAlumno().addItemListener(this);
		vista.getCbCurso().addItemListener(this);
	}

	//Implementamos los métodos de las interfaces de eventos
	@Override
	public void actionPerformed(ActionEvent e) {
		Object objeto = e.getSource();
		if (objeto == vista.getSalir()) {
			salir();
		} else if (objeto == vista.getCreate()) {
			create();
		} else if (objeto == vista.getRead()) {
			read();
		} else if (objeto == vista.getUpdate()) {
			update();
		} else if (objeto == vista.getDelete()) {
			delete();
		} else if (objeto == vista.getSearch()) {
			search();
		} else if (objeto == vista.getPrimero()) {
			primero();
		} else if (objeto == vista.getAnterior()) {
			anterior();
		} else if (objeto == vista.getSiguiente()) {
			siguiente();
		} else if (objeto == vista.getUltimo()) {
			ultimo();
		} else if (objeto == vista.getGuardar()) {
			guardar(chivato);
		} else if (objeto == vista.getCancelar()) {
			cancelar();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		Object objeto = e.getSource();
		if (objeto == vista.getSalir()) {
			vista.getInfo().setText("Vuelve al menú principal");
		} else if (objeto == vista.getCreate()) {
			vista.getInfo().setText("Permite dar de alta una matrícula");
		} else if (objeto == vista.getRead()) {
			vista.getInfo().setText("Permite navegar la lista de matrículas");
		} else if (objeto == vista.getUpdate()) {
			vista.getInfo().setText("Permite actualizar los datos de la matrícula");
		} else if (objeto == vista.getDelete()) {
			vista.getInfo().setText("Borra la matrícula en pantalla");
		} else if (objeto == vista.getSearch()) {
			vista.getInfo().setText("Busca por ID. 'Guardar' para buscar. 'Cancelar' para salir");
		} else if (objeto == vista.getPrimero()) {
			vista.getInfo().setText("Muestra la primera matrícula de la lista");
		} else if (objeto == vista.getAnterior()) {
			vista.getInfo().setText("Muestra la matrícula anterior");
		} else if (objeto == vista.getSiguiente()) {
			vista.getInfo().setText("Muestra la matrícula siguiente");
		} else if (objeto == vista.getUltimo()) {
			vista.getInfo().setText("Muestra la última matrícula de la lista");
		} else if (objeto == vista.getGuardar()) {
			vista.getInfo().setText("Guarda la información");
		} else if (objeto == vista.getCancelar()) {
			vista.getInfo().setText("Cancela la operación en curso");
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Object objeto = e.getSource();
		if (objeto == vista.getSalir() || objeto == vista.getCreate()
				|| objeto == vista.getRead() || objeto == vista.getUpdate()
				|| objeto == vista.getDelete() || objeto == vista.getSearch()
				|| objeto == vista.getPrimero() || objeto == vista.getAnterior()
				|| objeto == vista.getSiguiente() || objeto == vista.getUltimo()
				|| objeto == vista.getGuardar() || objeto == vista.getCancelar()) {
			vista.getInfo().setText("MENÚ MATRÍCULA");
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		//No hacer nada
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		//No hacer nada
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		//No hacer nada
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		Object objeto = e.getSource();
		if (objeto == vista.getCbAlumno()) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				String combo = (String) vista.getCbAlumno().getSelectedItem();
				StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
				int id = Integer.parseInt(stokenizer.nextToken());
				Iterator it = alumnos.iterator();
				while (it.hasNext()) {
					Alumnos alumno = (Alumnos) it.next();
					if (alumno.getIda() == id) {
						vista.getIdA().setText(Integer.toString(alumno.getIda()));
						vista.getTdni().setText(alumno.getDni());
						vista.getTedad().setText(Integer.toString(alumno.getEdad()));
						vista.getTemail().setText(alumno.getEmail());
					}
				}
			}
		} else if (objeto == vista.getCbCurso()) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				String combo = (String) vista.getCbCurso().getSelectedItem();
				StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
				int id = Integer.parseInt(stokenizer.nextToken());
				cursos = modelo.readCurso();
				Iterator it = cursos.iterator();
				while (it.hasNext()) {
					Cursos curso = (Cursos) it.next();
					if (curso.getIdc() ==  id) {
						vista.getIdC().setText(Integer.toString(curso.getIdc()));
						vista.getThoras().setText(Integer.toString(curso.getHoras()));
					}
				}
			}
		}
	}

	//Configuramos el comportamiento del menú CRUD    
	private void create() {
		chivato = "create";
		vista.getIdM().setEnabled(false);
		vista.getTfecha().setEnabled(true);
		vista.getCbAlumno().setEnabled(true);
		vista.getCbCurso().setEnabled(true);
		vista.getGuardar().setEnabled(true);
		vista.getCancelar().setEnabled(true);
		vista.getRead().setEnabled(false);
		vista.getSearch().setEnabled(false);
	}

	private void read() {
		matriculas = modelo.readMatricula();
		vista.getPrimero().setEnabled(true);
		vista.getAnterior().setEnabled(true);
		vista.getSiguiente().setEnabled(true);
		vista.getUltimo().setEnabled(true);
		vista.getCreate().setEnabled(false);
		vista.getRead().setEnabled(false);
		vista.getSearch().setEnabled(false);
		vista.getUpdate().setEnabled(true);
		vista.getDelete().setEnabled(true);
		vista.getCancelar().setEnabled(true);
		primero();
	}

	private void update() {
		chivato = "update";
		vista.getCbAlumno().setEnabled(true);
		vista.getCbCurso().setEnabled(true);
		vista.getTfecha().setEnabled(true);
		vista.getGuardar().setEnabled(true);
		vista.getCancelar().setEnabled(true);
		vista.getPrimero().setEnabled(false);
		vista.getAnterior().setEnabled(false);
		vista.getSiguiente().setEnabled(false);
		vista.getUltimo().setEnabled(false);
	}

	private void delete() {
		Matriculas matricula = alta();
		modelo.delete(matricula);
		cancelar();
	}

	private void search() {
		chivato = "search";
		vista.getIdM().setEnabled(true);
		vista.getGuardar().setEnabled(true);
		vista.getCancelar().setEnabled(true);
		vista.getCreate().setEnabled(false);
		vista.getRead().setEnabled(false);
	}

	//Configuramos el comportamiento del menú de navegabilidad
	private void primero() {
		if (matriculas != null) {
			indice = 0;
			actual = ((Matriculas) matriculas.get(indice));
			vista.getIdM().setText(Integer.toString(actual.getIdm()));
			vista.getTfecha().setDate(actual.getFecha());
			vista.getIdA().setText(Integer.toString(actual.getAlumnos().getIda()));
			cargaAlumno();
			vista.getIdC().setText(Integer.toString(actual.getCursos().getIdc()));
			cargaCurso();
		} else {
			actual = null;
			indice = -1;
		}
	}

	private void anterior() {
		if (indice != 0) {
			indice -= 1;
			actual = ((Matriculas) matriculas.get(indice));
			vista.getIdM().setText(Integer.toString(actual.getIdm()));
			vista.getTfecha().setDate(actual.getFecha());
			vista.getIdA().setText(Integer.toString(actual.getAlumnos().getIda()));
			cargaAlumno();
			vista.getIdC().setText(Integer.toString(actual.getCursos().getIdc()));
			cargaCurso();
		}
	}

	private void siguiente() {
		if (indice != matriculas.size() - 1) {
			indice += 1;
			actual = ((Matriculas) matriculas.get(indice));
			vista.getIdM().setText(Integer.toString(actual.getIdm()));
			vista.getTfecha().setDate(actual.getFecha());
			vista.getIdA().setText(Integer.toString(actual.getAlumnos().getIda()));
			cargaAlumno();
			vista.getIdC().setText(Integer.toString(actual.getCursos().getIdc()));
			cargaCurso();
		}
	}

	private void ultimo() {
		indice = (matriculas.size() - 1);
		actual = ((Matriculas) matriculas.get(indice));
		vista.getIdM().setText(Integer.toString(actual.getIdm()));
		vista.getTfecha().setDate(actual.getFecha());
		vista.getIdA().setText(Integer.toString(actual.getAlumnos().getIda()));
		cargaAlumno();
		vista.getIdC().setText(Integer.toString(actual.getCursos().getIdc()));
		cargaCurso();
	}

	//Configuramos el comportamiento de los botones guardar, cancelar y salir
	private void guardar(String chivato) {
		Matriculas matricula = new Matriculas();
		Funciones f = new Funciones();
		switch (chivato) {
			case "create":
				try {
					matricula = alta();
					if (matricula.getFecha() == null) {
						vista.getTfecha().setDate(new Date());
						matricula.setFecha(new Date());
					}
					modelo.create(matricula);
					cancelar();
				} catch (Exception e) {
					f.error("Ha ocurrido un error.\n"
							+ "Error: " + e.getMessage());
					e.printStackTrace();
				}
				break;
			case "update":
				try {
					matricula = alta();
					modelo.update(matricula);
					cancelar();
				} catch (Exception e) {
					f.error("Ha ocurrido un error.\n"
							+ "Error: " + e.getMessage());
					e.printStackTrace();
				}
				break;
			case "search":
				try {
					matricula.setIdm(Integer.parseInt(vista.getIdM().getText()));
					Matriculas m = modelo.search(matricula);
					vista.getTfecha().setDate(m.getFecha());
					vista.getIdA().setText(Integer.toString(m.getAlumnos().getIda()));
					cargaAlumno();
					vista.getIdC().setText(Integer.toString(m.getCursos().getIdc()));
					cargaCurso();
				} catch (Exception e) {
					f.error("Ha ocurrido un error.\n"
							+ "Error: " + e.getMessage());
					e.printStackTrace();
				}
				break;
		}
	}

	private void cancelar() {
		//Deshabilitamos campos de texto 
		vista.getIdM().setEnabled(false);
		vista.getTfecha().setEnabled(false);
		vista.getIdA().setEnabled(false);
		vista.getCbAlumno().setEnabled(false);
		vista.getTdni().setEnabled(false);
		vista.getTedad().setEnabled(false);
		vista.getTemail().setEnabled(false);
		vista.getIdC().setEnabled(false);
		vista.getCbCurso().setEnabled(false);
		vista.getThoras().setEnabled(false);
		//Limpiamos campos de texto
		vista.getIdM().setText("");
		vista.getTfecha().setDate(null);
		vista.getIdA().setText("");
		vista.getCbAlumno().setSelectedItem(null);
		vista.getTdni().setText("");
		vista.getTedad().setText("");
		vista.getTemail().setText("");
		vista.getIdC().setText("");
		vista.getCbCurso().setSelectedItem(null);
		vista.getThoras().setText("");
		//Configuramos guardar, cancelar
		vista.getGuardar().setEnabled(false);
		vista.getCancelar().setEnabled(false);
		//Configuramos CRUD
		vista.getCreate().setEnabled(true);
		vista.getRead().setEnabled(true);
		vista.getUpdate().setEnabled(false);
		vista.getDelete().setEnabled(false);
		vista.getSearch().setEnabled(true);
		//Configuramos navegabilidad
		vista.getPrimero().setEnabled(false);
		vista.getAnterior().setEnabled(false);
		vista.getSiguiente().setEnabled(false);
		vista.getUltimo().setEnabled(false);
	}

	private void salir() {
		vista.dispose();
	}

	//Otras funciones
	private Matriculas alta() {
		String nombre = "";
		String nombreCurso = "";
		Matriculas matricula = null;
		//Datos matrícula
		Date fecha = vista.getTfecha().getDate();
		//Datos alumno
		int idA = Integer.parseInt(vista.getIdA().getText());
		Iterator it = (modelo.readAlumno()).iterator();
		while (it.hasNext()) {
			Alumnos a = (Alumnos) it.next();
			if (a.getIda() == idA) {
				nombre = a.getNombrea();
			}
		}
		String dni = vista.getTdni().getText();
		int edad = Integer.parseInt(vista.getTedad().getText());
		String email = vista.getTemail().getText();
		//Datos curso
		int idC = Integer.parseInt(vista.getIdC().getText());
		it = (modelo.readCurso()).iterator();
		while (it.hasNext()) {
			Cursos c = (Cursos) it.next();
			if (c.getIdc() == idC) {
				nombreCurso = c.getNombrec();
			}
		}
		int horas = Integer.parseInt(vista.getThoras().getText());
		Alumnos alumno = new Alumnos(idA, nombre, dni, edad, email);
		Cursos curso = new Cursos(idC, nombreCurso, horas);
		if (!vista.getIdM().getText().equals("")){
			int idM = Integer.parseInt(vista.getIdM().getText());
			matricula = new Matriculas(idM, alumno, curso, fecha);
		} else {
			matricula = new Matriculas(0, alumno, curso, fecha);
		}
		return matricula;
	}

	private void cargaCombo() {
		alumnos = null;
		alumnos = modelo.readAlumno();
		Iterator it = alumnos.iterator();
		vista.getCbAlumno().removeAllItems();
		vista.getCbAlumno().addItem(null);
		while (it.hasNext()) {
			Alumnos alumno = (Alumnos) it.next();
			int idAlumno = alumno.getIda();
			String nombre = alumno.getNombrea();
			vista.getCbAlumno().addItem(idAlumno + " - " + nombre);
		}
		cursos = null;
		cursos = modelo.readCurso();
		vista.getCbCurso().removeAllItems();
		vista.getCbCurso().addItem(null);
		it = cursos.iterator();
		while (it.hasNext()) {
			Cursos curso = (Cursos) it.next();
			int idCurso = curso.getIdc();
			String nombreCurso = curso.getNombrec();
			vista.getCbCurso().addItem(idCurso + " - " + nombreCurso);
		}
	}

	private void cargaAlumno() {
		int id;
		alumnos = modelo.readAlumno();
		Iterator it;
		Alumnos alumno;
		if (!vista.getIdA().getText().equals("")) {
			id = Integer.parseInt(vista.getIdA().getText());
			it = alumnos.iterator();
			while (it.hasNext()) {
				alumno = (Alumnos) it.next();
				if (alumno.getIda() == id) {
					vista.getIdA().setText(Integer.toString(alumno.getIda()));
					vista.getCbAlumno().setSelectedItem(alumno.getIda() + " - " + alumno.getNombrea());
					vista.getTdni().setText(alumno.getDni());
					vista.getTedad().setText(Integer.toString(alumno.getEdad()));
					vista.getTemail().setText(alumno.getEmail());
				}
			}
		} else if (!vista.getCbAlumno().getSelectedItem().equals("")) {
			String combo = (String) vista.getCbAlumno().getSelectedItem();
			StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
			id = Integer.parseInt(stokenizer.nextToken());
			it = alumnos.iterator();
			while (it.hasNext()) {
				alumno = (Alumnos) it.next();
				if (alumno.getIda() == id) {
					vista.getIdA().setText(Integer.toString(alumno.getIda()));
					vista.getTdni().setText(alumno.getDni());
					vista.getTedad().setText(Integer.toString(alumno.getEdad()));
					vista.getTemail().setText(alumno.getEmail());
				}
			}
		}
	}

	private void cargaCurso() {
		int id;
		cursos = modelo.readCurso();
		Iterator it;
		Cursos curso;

		if (!vista.getIdC().getText().equals("")) {
			id = Integer.parseInt(vista.getIdC().getText());
			it = cursos.iterator();
			while (it.hasNext()) {
				curso = (Cursos) it.next();
				if (curso.getIdc() == id) {
					vista.getIdC().setText(Integer.toString(curso.getIdc()));
					vista.getCbCurso().setSelectedItem(curso.getIdc() + " - " + curso.getNombrec());
					vista.getThoras().setText(Integer.toString(curso.getHoras()));
				}
			}
		} else if (!vista.getCbCurso().getSelectedItem().equals("")) {
			String combo = (String) vista.getCbCurso().getSelectedItem();
			StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
			id = Integer.parseInt(stokenizer.nextToken());
			cursos = modelo.readCurso();
			it = cursos.iterator();
			while (it.hasNext()) {
				curso = (Cursos) it.next();
				if (curso.getIdc() == id) {
					vista.getIdC().setText(Integer.toString(curso.getIdc()));
					vista.getThoras().setText(Integer.toString(curso.getHoras()));
				}
			}
		}
	}
}