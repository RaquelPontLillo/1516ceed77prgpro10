package org.ceedcv.ceed77prgpro10.controlador;

import java.io.IOException;
import org.ceedcv.ceed77prgpro10.modelo.IModelo;
import org.ceedcv.ceed77prgpro10.vista.VistaLogin;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Main {
    public static void main (String[] args) throws IOException {		
        IModelo modelo = null;
        VistaLogin vista = new VistaLogin();
        ControladorLogin controlador = new ControladorLogin(modelo, vista);
    }
}