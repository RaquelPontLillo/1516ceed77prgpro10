package org.ceedcv.ceed77prgpro10.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceedcv.ceed77prgpro10.modelo.IModelo;
import org.ceedcv.ceed77prgpro10.modelo.Usuario;
import org.ceedcv.ceed77prgpro10.vista.Funciones;
import org.ceedcv.ceed77prgpro10.vista.VistaGrafica;
import org.ceedcv.ceed77prgpro10.vista.VistaLogin;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ControladorLogin implements ActionListener{
	private final Funciones f = new Funciones();
	private final IModelo modelo;
	private final VistaLogin vista;
	
    public ControladorLogin(IModelo m, VistaLogin v) throws IOException {
		modelo = m;
		vista = v;
		
		//Listeners
		vista.getjButton1().addActionListener(this);
		
		vista.setLocationRelativeTo(null);
    }
	
	private boolean validarAcceso() {
		Usuario u = new Usuario();
		String user = vista.getjTextField1().getText();
		String pass = vista.getjPasswordField1().getText();
		return user.equals(u.getUsuario()) && pass.equals(u.getPassword());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object objeto = e.getSource();
		if (objeto == vista.getjButton1()) {
			try {
				acceder();
			} catch (IOException ex) {
				Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
	
	private void acceder() throws IOException {
		boolean valido = validarAcceso();
		if (!valido) {
			f.error("El usuario o la contraseña no coinciden o son erróneos.");
		} else {
			//Cargamos el controlador principal
			vista.setVisible(false);
			VistaGrafica vg = null;
			Controlador c = new Controlador(modelo, vg);
		}
	}
}