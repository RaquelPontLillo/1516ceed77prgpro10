package org.ceedcv.ceed77prgpro10.modelo;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class Persona {

	//Variables de la clase
	protected int idPersona;
	protected String nombre;
	protected String dni;

	//Constructores de la clase
	public Persona() {

	}

	public Persona(int idP, String n, String d) {
		idPersona = idP;
		nombre = n;
		dni = d;
	}

	//Métodos de la clase
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setId(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDni() {
		return dni;
	}

	public int getId() {
		return idPersona;
	}
}
