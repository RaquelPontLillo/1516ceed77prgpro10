package org.ceedcv.ceed77prgpro10.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceedcv.ceed77prgpro10.vista.Funciones;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class ModeloFichero implements IModelo {
	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	private final Funciones f = new Funciones();
	private final File alumnos = new File("FicheroAlumnos.csv"); //creamos el archivo para guardar los alumnos
	private final File cursos = new File("FicheroCursos.csv"); //creamos el archivo para guardar los cursos
	private final File matriculas = new File("FicheroMatriculas.csv"); //creamos el archivo para guardar las matrículas
	private FileWriter fwa, fwc, fwm;
	private BufferedReader bra, brc, brm;
	private FileReader fra, frc, frm;
	private String string;

	public ModeloFichero() {
		if (!alumnos.exists() || !cursos.exists() || !matriculas.exists()) {
			f.error("No existe la base de datos CSV. \n"
					+ "Deberás instalarla previamente para usar el programa.");
		}
	}

	@Override
	public void create(Alumnos alumno) {
		alumno.setIda(idAlumno());
		try {
			fwa = new FileWriter(alumnos, true); //creamos un objeto filewriter para utilizarlo sobre el fichero de alumnos
			fwa.write(alumno.getIda() + ";" + alumno.getNombrea() + ";" + alumno.getDni() + ";" + alumno.getEdad()
					+ ";" + alumno.getEmail() + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
			fwa.close(); //cerramos el fichero de alumnos
			f.info("Alumno dado de alta con éxito."); //mostramos mensaje informativo
		} catch (IOException e) { //capturamos la excepción 
			Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, e); //mostramos el error
		}
	}

	@Override
	public void create(Cursos curso) {
		curso.setIdc(idCurso());
		try {
			fwc = new FileWriter(cursos, true); //creamos un objeto filewriter para utilizarlo sobre el fichero de cursos
			fwc.write(curso.getIdc() + ";" + curso.getNombrec() + ";"
					+ curso.getHoras() + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
			fwc.close(); //cerramos el fichero de cursos
			f.info("Curso dado de alta con éxito."); //mostramos mensaje informativo
		} catch (IOException ex) { //capturamos la excepción 
			Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex); //mostramos el error
		}
	}

	@Override
	public void create(Matriculas matricula) {
		matricula.setIdm(idMatricula());
		try {
			fwm = new FileWriter(matriculas, true); //creamos un objeto filewriter para utilizarlo sobre el fichero matriculas
			fwm.write(matricula.getIdm() + ";" + sdf.format(matricula.getFecha()) + ";" + matricula.getAlumnos().getIda() + ";"
					+ matricula.getCursos().getIdc() + ";\r\n");
			//escribimos el objeto en el fichero separando los datos con ;
			fwm.close(); //cerramos el fichero de alumnos
			f.info("Matrícula dada de alta con éxito."); //mostramos mensaje informativo
		} catch (IOException ex) { //capturamos la excepción 
			Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex); //mostramos el error
		}
	}

	@Override
	public ArrayList readAlumno() {
		ArrayList array = new ArrayList();
		string = "";
		try {
			fra = new FileReader(alumnos);
			bra = new BufferedReader(fra);
			string = bra.readLine();
			while (string != null) {
				Alumnos alumno = obtenerAlumno(string);
				array.add(alumno);
				string = bra.readLine();
			}
			fra.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return array;
	}

	@Override
	public ArrayList readCurso() {
		ArrayList array = new ArrayList();
		string = "";
		try {
			frc = new FileReader(cursos);
			brc = new BufferedReader(frc);
			string = brc.readLine();
			while (string != null) {
				Cursos curso = obtenerCurso(string);
				array.add(curso);
				string = brc.readLine();
			}
			frc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return array;
	}

	@Override
	public ArrayList readMatricula() {
		ArrayList array = new ArrayList();
		string = "";
		try {
			frm = new FileReader(matriculas);
			brm = new BufferedReader(frm);
			string = brm.readLine();
			while (string != null) {
				Matriculas matricula = obtenerMatricula(string);
				array.add(matricula);
				string = brm.readLine();
			}
			frm.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return array;
	}

	@Override
	public void update(Alumnos alumno) {
		File temp = new File("temp.csv");
		string = "";
		try {
			fwa = new FileWriter(temp, true);
			fra = new FileReader(alumnos);
			bra = new BufferedReader(fra);
			string = bra.readLine();
			while (string != null) {
				Alumnos a = obtenerAlumno(string);
				if (Objects.equals(alumno.getIda(), a.getIda())) {
					fwa.write(alumno.getIda() + ";" + alumno.getNombrea() + ";" + alumno.getDni() + ";" + alumno.getEdad()
							+ ";" + alumno.getEmail() + ";\r\n");
				} else {
					fwa.write(a.getIda() + ";" + a.getNombrea() + ";" + a.getDni() + ";" + a.getEdad() + ";"
							+ a.getEmail() + ";\r\n");
				}
				string = bra.readLine();
			}
			fwa.close();
			fra.close();
			f.info("Alumno actualizado con éxito.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		alumnos.delete();
		temp.renameTo(new File("FicheroAlumnos.csv"));
	}

	@Override
	public void update(Cursos curso) {
		File temp = new File("temp.csv");
		string = "";
		try {
			fwc = new FileWriter(temp, true);
			frc = new FileReader(cursos);
			brc = new BufferedReader(frc);
			string = brc.readLine();
			while (string != null) {
				Cursos c = obtenerCurso(string);
				if (Objects.equals(curso.getIdc(), c.getIdc())) {
					fwc.write(curso.getIdc() + ";" + curso.getNombrec() + ";" + curso.getHoras() + ";\r\n");
				} else {
					fwc.write(c.getIdc() + ";" + c.getNombrec() + ";" + c.getHoras() + ";\r\n");
				}
				string = brc.readLine();
			}
			fwc.close();
			fwc.close();
			f.info("Curso actualizado con éxito.");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		cursos.delete();
		temp.renameTo(new File("FicheroCursos.csv"));
	}

	@Override
	public void update(Matriculas matricula) {
		File temp = new File("temp.csv");
		string = "";
		try {
			fwm = new FileWriter(temp, true);
			frm = new FileReader(matriculas);
			brm = new BufferedReader(frm);
			string = brm.readLine();
			while (string != null) {
				Matriculas m = obtenerMatricula(string);
				if (Objects.equals(matricula.getIdm(), m.getIdm())) {
					fwm.write(matricula.getIdm() + ";" + sdf.format(matricula.getFecha()) + ";" + matricula.getAlumnos().getIda() + ";"
							+ matricula.getCursos().getIdc() + ";\r\n");
				} else {
					fwm.write(m.getIdm() + ";" + sdf.format(m.getFecha()) + ";" + m.getAlumnos().getIda() + ";"
							+ m.getCursos().getIdc() + ";\r\n");
				}
				string = brm.readLine();
			}
			fwm.close();
			frm.close();
			f.info("Matrícula actualizada con éxito.");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		matriculas.delete();
		temp.renameTo(new File("FicheroMatriculas.csv"));
	}

	@Override
	public void delete(Alumnos alumno) {
		File temp = new File("temp.csv");
		string = "";
		try {
			fwa = new FileWriter(temp, true);
			fra = new FileReader(alumnos);
			bra = new BufferedReader(fra);
			string = bra.readLine();
			while (string != null) {
				Alumnos a = obtenerAlumno(string);
				if (!Objects.equals(alumno.getIda(), a.getIda())) {
					fwa.write(a.getIda() + ";" + a.getNombrea() + ";" + a.getDni() + ";" + a.getEdad() + ";" + a.getEmail() + ";\r\n");
				}
				string = bra.readLine();
			}
			fwa.close();
			fwa.close();
			f.info("Alumno borrado con éxito.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		alumnos.delete();
		temp.renameTo(new File("FicheroAlumnos.csv"));
	}

	@Override
	public void delete(Cursos curso) {
		File temp = new File("temp.csv");
		string = "";
		try {
			fwc = new FileWriter(temp, true);
			frc = new FileReader(cursos);
			brc = new BufferedReader(frc);
			string = brc.readLine();
			while (string != null) {
				Cursos c = obtenerCurso(string);
				if (!Objects.equals(curso.getIdc(), c.getIdc())) {
					fwc.write(c.getIdc() + ";" + c.getNombrec() + ";" + c.getHoras() + ";\r\n");
				}
				string = brc.readLine();
			}
			fwc.close();
			frc.close();
			f.info("Curso borrado con éxito.");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		cursos.delete();
		temp.renameTo(new File("FicheroCursos.csv"));
	}

	@Override
	public void delete(Matriculas matricula) {
		File temp = new File("temp.csv");
		string = "";
		try {
			fwm = new FileWriter(temp, true);
			frm = new FileReader(matriculas);
			brm = new BufferedReader(frm);
			string = brm.readLine();
			while (string != null) {
				Matriculas m = obtenerMatricula(string);
				if (!Objects.equals(matricula.getIdm(), m.getIdm())) {
					fwm.write(m.getIdm() + ";" + sdf.format(m.getFecha()) + ";" + m.getAlumnos().getIda() + ";"
							+ m.getCursos().getIdc() + ";\r\n");
				}
				string = brm.readLine();
			}
			fwm.close();
			frm.close();
			f.info("Matricula borrada con éxito.");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		matriculas.delete();
		temp.renameTo(new File("FicheroMatriculas.csv"));
	}

	@Override
	public Alumnos search(Alumnos alumno) {
		ArrayList array = readAlumno();
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {
			Alumnos a = (Alumnos) iterator.next();
			if (Objects.equals(alumno.getIda(), a.getIda())) {
				return a;
			}
		}
		return null;
	}

	@Override
	public Cursos search(Cursos curso) {
		ArrayList array = readCurso();
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {
			Cursos c = (Cursos) iterator.next();
			if (Objects.equals(curso.getIdc(), c.getIdc())) {
				return c;
			}
		}
		return null;
	}

	@Override
	public Matriculas search(Matriculas matricula) {
		ArrayList array = readMatricula();
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {
			Matriculas m = (Matriculas) iterator.next();
			if (Objects.equals(matricula.getIdm(), m.getIdm())) {
				return m;
			}
		}
		return null;
	}

	//Funciones de la base de datos
	@Override
	public void crearBd() {
		try {
			if (!alumnos.exists()) {
				fwa = new FileWriter(alumnos);
				fwa.close();
			}
			if (!cursos.exists()) {
				fwc = new FileWriter(cursos);
				fwc.close();
			}
			if (!matriculas.exists()) {
				fwm = new FileWriter(matriculas);
				fwm.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			f.error("Ha ocurrido un error durante la creación de los ficheros CSV. \n"
					+ "Error: " + e.getMessage());
		}
	}

	@Override
	public void crearTablas() {
		//No procede
	}

	@Override
	public void crearDatos() {
		try {
			Alumnos a1 = new Alumnos(1, "Juan Palomares", "44522840-S", 45, "juan@hotmail.com");
			Alumnos a2 = new Alumnos(2, "Felipe Ortiz", "44522840-S", 56, "felip@gmail.com");
			Alumnos a3 = new Alumnos(3, "Jose Sánchez", "44522840-S", 42, "joselito@hotmail.com");
			Alumnos a4 = new Alumnos(4, "Felipe Ortiz", "44522840-S", 56, "felip@gmail.com");
			Alumnos a5 = new Alumnos(5, "Miguel Torres", "44522840-S", 45, "miguel@hotmail.com");
			Alumnos a6 = new Alumnos(6, "María Furió", "44522840-S", 26, "maria@gmail.com");
			Alumnos a7 = new Alumnos(7, "Marta Moreno", "44522840-S", 28, "marta@mailinator.com");
			Alumnos a8 = new Alumnos(8, "Esther Marco", "44522840-S", 35, "esther@gva.es");
			create(a1);
			create(a2);
			create(a3);
			create(a4);
			create(a5);
			create(a6);
			create(a7);
			create(a8);
			Cursos c1 = new Cursos(1, "Matemáticas", 40);
			Cursos c2 = new Cursos(2, "Lengua", 50);
			Cursos c3 = new Cursos(3, "Inglés", 30);
			Cursos c4 = new Cursos(4, "Biología", 45);
			create(c1);
			create(c2);
			create(c3);
			create(c4);
			Matriculas m1 = new Matriculas(1, a1, c2, sdf.parse("2016/04/11"));
			Matriculas m2 = new Matriculas(2, a2, c1, sdf.parse("2016/04/21"));
			Matriculas m3 = new Matriculas(3, a3, c3, sdf.parse("2016/03/25"));
			Matriculas m4 = new Matriculas(4, a4, c3, sdf.parse("2016/03/07"));
			Matriculas m5 = new Matriculas(5, a5, c4, sdf.parse("2016/04/15"));
			Matriculas m6 = new Matriculas(6, a6, c2, sdf.parse("2016/02/12"));
			Matriculas m7 = new Matriculas(7, a7, c1, sdf.parse("2016/03/09"));
			Matriculas m8 = new Matriculas(8, a8, c4, sdf.parse("2016/03/07"));
			Matriculas m9 = new Matriculas(9, a1, c4, sdf.parse("2016/02/07"));
			Matriculas m10 = new Matriculas(10, a2, c3, sdf.parse("2016/01/17"));
			Matriculas m11 = new Matriculas(11, a3, c1, sdf.parse("2016/01/26"));
			Matriculas m12 = new Matriculas(12, a4, c2, sdf.parse("2016/05/07"));
			Matriculas m13 = new Matriculas(13, a5, c3, sdf.parse("2016/03/21"));
			Matriculas m14 = new Matriculas(14, a6, c1, sdf.parse("2016/04/07"));
			Matriculas m15 = new Matriculas(15, a7, c4, sdf.parse("2016/02/23"));
			Matriculas m16 = new Matriculas(16, a8, c1, sdf.parse("2016/02/12"));
			Matriculas m17 = new Matriculas(17, a3, c4, sdf.parse("2016/03/03"));
			Matriculas m18 = new Matriculas(18, a8, c3, sdf.parse("2016/01/14"));
			Matriculas m19 = new Matriculas(19, a5, c1, sdf.parse("2016/03/19"));
			Matriculas m20 = new Matriculas(20, a1, c1, sdf.parse("2016/05/01"));
			create(m1);
			create(m2);
			create(m3);
			create(m4);
			create(m5);
			create(m6);
			create(m7);
			create(m8);
			create(m9);
			create(m10);
			create(m11);
			create(m12);
			create(m13);
			create(m14);
			create(m15);
			create(m16);
			create(m17);
			create(m18);
			create(m19);
			create(m20);
		} catch (ParseException ex) {
			Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void conectarBd() {
		//No procede
	}

	@Override
	public void desconectarBd() {
		//No procede
	}

	@Override
	public void eliminarBd() {
		alumnos.delete();
		cursos.delete();
		matriculas.delete();
		f.info("Ficheros CSV eliminados correctamente.");
	}

	//Otras funciones
	private int idAlumno() {
		int idActual = 0;
		string = "";
		if (alumnos.exists()) {
			try {
				fra = new FileReader(alumnos);
				bra = new BufferedReader(fra);
				string = bra.readLine();
				while (string != null) {
					Alumnos alumno = obtenerAlumno(string);
					int id = alumno.getIda();
					if (id > idActual) {
						idActual = id;
					}
					string = bra.readLine();
				}
				fra.close();
			} catch (FileNotFoundException lfe) {
				f.error("No se encuentra el fichero" + alumnos.getName());
			} catch (IOException lie) {
				f.error("Ha ocurrido un error. \n"
						+ "Error" + lie.getMessage());
			}
		}
		return idActual + 1;
	}

	private int idCurso() {
		int idActual = 0;
		string = "";
		if (cursos.exists()) {
			try {
				frc = new FileReader(cursos);
				brc = new BufferedReader(frc);
				string = brc.readLine();
				while (string != null) {
					Cursos curso = obtenerCurso(string);
					int id = curso.getIdc();
					if (id > idActual) {
						idActual = id;
					}
					string = brc.readLine();
				}
				frc.close();
			} catch (FileNotFoundException lfe) {
				f.error("No se encuentra el fichero" + cursos.getName());
			} catch (IOException lie) {
				f.error("Ha ocurrido un error. \n"
						+ "Error" + lie.getMessage());
			}
		}
		return idActual + 1;
	}

	private int idMatricula() {
		int idActual = 0;
		string = "";
		if (matriculas.exists()) {
			try {
				frm = new FileReader(matriculas);
				brm = new BufferedReader(frm);
				string = brm.readLine();
				while (string != null) {
					Matriculas m = obtenerMatricula(string);
					int id = m.getIdm();
					if (id > idActual) {
						idActual = id;
					}
					string = brm.readLine();
				}
				frm.close();
			} catch (FileNotFoundException lfe) {
				f.error("No se encuentra el fichero" + matriculas);
			} catch (IOException lie) {
				f.error("Ha ocurrido un error. \n"
						+ "Error" + lie.getMessage());
			}
		}
		return idActual + 1;
	}

	private Alumnos obtenerAlumno(String s) {
		StringTokenizer stokenizer = new StringTokenizer(s, ";");
		int idAlumno = Integer.parseInt(stokenizer.nextToken());
		String nAlumno = stokenizer.nextToken();
		String dni = stokenizer.nextToken();
		int edad = Integer.parseInt(stokenizer.nextToken());
		String email = stokenizer.nextToken();
		Alumnos a = new Alumnos(idAlumno, nAlumno, dni, edad, email);
		return a;
	}

	private Cursos obtenerCurso(String s) {
		StringTokenizer stokenizer = new StringTokenizer(s, ";");
		int idCurso = Integer.parseInt(stokenizer.nextToken());
		String nombre = stokenizer.nextToken();
		int horas = Integer.parseInt(stokenizer.nextToken());
		Cursos c = new Cursos(idCurso, nombre, horas);
		return c;
	}

	private Matriculas obtenerMatricula(String s) {
		StringTokenizer stokenizer = new StringTokenizer(s, ";");
		//Recogemos las variables del objeto matricula
		int idMatricula = Integer.parseInt(stokenizer.nextToken());
		Date fecha = null;
		try {
			fecha = sdf.parse(stokenizer.nextToken());
		} catch (ParseException pe) {
			f.error("No se ha podido convertir el campo Date a String. \n"
					+ "Error: " + pe.getMessage());
		}
		//Recogemos las variables del objeto alumno y lo buscamos
		int idAlumno = Integer.parseInt(stokenizer.nextToken());
		Alumnos a = new Alumnos(idAlumno, "", "", 0, "");
		Alumnos alumno = search(a);
		//Recogemos las variables del objeto curso y lo buscamos
		int idCurso = Integer.parseInt(stokenizer.nextToken());
		Cursos c = new Cursos(idCurso, "", 0);
		Cursos curso = search(c);
		//Montamos el objeto matrícula
		Matriculas m = new Matriculas(idMatricula, alumno, curso, fecha);
		return m;
	}
}
