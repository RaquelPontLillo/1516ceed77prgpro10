package org.ceedcv.ceed77prgpro10.modelo;

import java.util.Date;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class Matricula {

	//Variables de la clase

	private int idMatricula;
	private Curso curso;
	private Alumno alumno;
	private Date fecha;

  //Constructores de la clase
	public Matricula(int idM) {
		idMatricula = idM;
	}

	public Matricula() {

	}

	public Matricula(int idM, Date fec, Curso cur, Alumno al) {
		idMatricula = idM;
		fecha = fec;
		curso = cur;
		alumno = al;
	}

  //Métodos de la clase
	//Getters
	public int getIdMatricula() {
		return idMatricula;
	}

	public Curso getCurso() {
		return curso;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public Date getFecha() {
		return fecha;
	}

	//Setters
	public void setIdMatricula(int idM) {
		idMatricula = idM;
	}

	public void setCurso(Curso idC) {
		curso = idC;
	}

	public void setAlumno(Alumno idA) {
		alumno = idA;
	}

	public void setFecha(Date fec) {
		fecha = fec;
	}
}
