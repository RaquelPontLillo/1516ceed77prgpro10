package org.ceedcv.ceed77prgpro10.modelo;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public interface IModelo {
    	//Alumno
	public void create(Alumnos alumno);
	public ArrayList<Alumnos> readAlumno();
	public void update(Alumnos alumno);
	public void delete(Alumnos alumno);
        public Alumnos search(Alumnos alumno);
	
	//Curso
	public void create(Cursos curso);
	public ArrayList<Cursos> readCurso();
	public void update(Cursos curso);
	public void delete(Cursos curso);
        public Cursos search(Cursos curso);
	
	//Matricula
	public void create(Matriculas matricula);
	public ArrayList<Matriculas> readMatricula();
	public void update(Matriculas matricula);
	public void delete(Matriculas matricula);
        public Matriculas search(Matriculas matricula);
        
        //Base de datos
        public void crearBd();
        public void crearTablas();
        public void crearDatos();
        public void conectarBd();
        public void desconectarBd();
        public void eliminarBd();
}