package org.ceedcv.ceed77prgpro10.modelo;

//import com.mysql.jdbc.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.PooledConnection;
import org.ceedcv.ceed77prgpro10.controlador.HibernateUtil;
import org.ceedcv.ceed77prgpro10.vista.Funciones;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ModeloHibernate implements IModelo {
	private final String bbdd = "1516ceed77prg";
	private final String usuario = "alumno";
	private final String password = "alumno";
	private final String jdbcUrl = "jdbc:mysql://localhost/" + bbdd;
	private final String driver = "com.mysql.jdbc.Driver";
	private final MysqlConnectionPoolDataSource mcpds = new MysqlConnectionPoolDataSource();
	private final Funciones f = new Funciones();
	private ArrayList alumnos, cursos, matriculas;
	private PooledConnection pc = null;
	private java.sql.Connection cn = null;
	private Statement st = null;
	private ResultSet rs = null;
	private String error, sql;
	
	public ModeloHibernate() {
		//Configuración del pool de conexiones
		mcpds.setUser(usuario);
		mcpds.setPassword(password);
		mcpds.setUrl(jdbcUrl);
	}
	
	@Override
	public void conectarBd() {
		try {
			pc = mcpds.getPooledConnection();
			cn = pc.getConnection();
			System.out.println("Base de datos " + bbdd + ": conectada");
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void desconectarBd() {
		try {
			System.out.println("Base de datos " + bbdd + ": desconectada");
			cn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	@Override
	public void crearBd() {
		st = null;
		error = null;
		try {
			Class.forName(driver).newInstance();
			cn = ((java.sql.Connection) DriverManager.getConnection("jdbc:mysql://localhost/", usuario, password));
			st = cn.createStatement();
			sql = "CREATE DATABASE IF NOT EXISTS " + bbdd + ";";
			System.out.println(sql);
			st.executeUpdate(sql);
			f.info("Base de datos " + bbdd + " creada correctamente");
		} catch (Exception e) {
			error = e.getMessage();
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
					error = e.getMessage();
				}
			}
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException se) {
					error = se.getMessage();
				}
			}
		}
		if (error != null) {
			f.error("Ha ocurrido un error durante la instalación \n "
					+ "Error: " + error);
		}
	}

	@Override
	public void crearTablas() {
		error = null;
		try {
			conectarBd();
			st = cn.createStatement();

			sql = "DROP TABLE IF EXISTS alumnos;";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "DROP TABLE IF EXISTS cursos;";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "DROP TABLE IF EXISTS matriculas;";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "CREATE TABLE IF NOT EXISTS `cursos` (\n"
					+ "`idc` int(5) NOT NULL AUTO_INCREMENT,\n"
					+ "`nombrec` varchar(35) NOT NULL,\n"
					+ "`horas` int(5),\n"
					+ "PRIMARY KEY (`idc`)\n"
					+ ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "CREATE TABLE IF NOT EXISTS `alumnos` (\n"
					+ "  `ida` int(5) NOT NULL AUTO_INCREMENT,\n"
					+ "  `nombrea` varchar(25),\n"
					+ "  `dni` varchar(11),\n"
					+ "  `edad` int(11) ,\n"
					+ "  `email` varchar(25),\n"
					+ "  PRIMARY KEY (`ida`)\n"
					+ ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "CREATE TABLE IF NOT EXISTS `matriculas` (\n"
					+ "  `idm` int(5) NOT NULL AUTO_INCREMENT,\n"
					+ "  `fecha` date,\n"
					+ "  `idc` int(5),\n"
					+ "  `ida` int(5),\n"
					+ "  PRIMARY KEY (`idm`),\n"
					+ "  FOREIGN KEY (`idc`) REFERENCES cursos(idc),\n"
					+ "  FOREIGN KEY (`ida`) REFERENCES alumnos(ida)\n"
					+ ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;";
			System.out.println(sql);
			st.executeUpdate(sql);

			f.info("Tablas creadas correctamente");
			st.close();
		} catch (SQLException ex) {
			error = ex.getMessage();
			Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
			f.error("Ha ocurrido un error durante la creación de las tablas. \n"
					+ "Error:" + error);
		}
		desconectarBd();
	}

	@Override
	public void crearDatos() {
		error = null;
		try {
			conectarBd();
			st = cn.createStatement();

			sql = "INSERT INTO cursos(idc,nombrec,horas) VALUES(1,'Lengua',20)\n";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "INSERT INTO cursos(idc,nombrec,horas) VALUES(2,'Matemáticas',40);\n";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "INSERT INTO alumnos(ida,nombrea,dni,edad,email) VALUES(1,'Juan','44522840-S',25,'juan@gmail.com');\n";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "INSERT INTO alumnos(ida,nombrea,dni,edad,email) VALUES(2,'Luis','44522840-S',39,'luis@gmail.com');\n";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "INSERT INTO matriculas(fecha,idc,ida) VALUES('2016-03-09',1,1);\n";
			System.out.println(sql);
			st.executeUpdate(sql);

			sql = "INSERT INTO matriculas(fecha,idc,ida) VALUES('2016-03-21',2,2);\n";
			System.out.println(sql);
			st.executeUpdate(sql);

			f.info("Datos creados correctamente");
			st.close();
		} catch (SQLException ex) {
			error = ex.getMessage();
			Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
			f.error("Ha ocurrido un error durante la inserción de datos en la base de datos. \n"
					+ "Error:" + error);
		}
		desconectarBd();
	}

	@Override
	public void eliminarBd() {
		cn = null;
		st = null;
		error = null;
		try {
			conectarBd();
			st = cn.createStatement();
			sql = "DROP DATABASE IF EXISTS " + bbdd + ";";
			System.out.println(sql);
			st.executeUpdate(sql);
		} catch (Exception e) {
			error = e.getMessage();
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
					error = e.getMessage();
				}
			}
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException se) {
					error = se.getMessage();
				}
			}
		}
	}

	@Override
	public void create(Alumnos alumno) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(alumno);
			session.getTransaction().commit();
			f.info("Alumno " + alumno.getNombrea() + " creado correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
		}
	}
	
	@Override
	public void create(Cursos curso) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(curso);
			session.getTransaction().commit();
			f.info("Curso " + curso.getNombrec() + " creado correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
		}
	}

	@Override
	public void create(Matriculas matricula) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(matricula);
			session.getTransaction().commit();
			f.info("Matrícula creada correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
		}
	}

	@Override
	public ArrayList readAlumno() {
		alumnos = new ArrayList();
		try {
			String hql = "from Alumnos";
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Query q = session.createQuery(hql);
			alumnos = (ArrayList) q.list();
			session.getTransaction().commit();
		} catch (HibernateException he) {
			he.printStackTrace();
		}
		return alumnos;
	}
	
	@Override
	public ArrayList readCurso() {
		cursos = new ArrayList();
		try {
			String hql = "from Cursos";
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Query q = session.createQuery(hql);
			cursos = (ArrayList) q.list();
			session.getTransaction().commit();
		} catch (HibernateException he) {
			he.printStackTrace();
		}
		return cursos;
	}
	
	@Override
	public ArrayList readMatricula() {
			matriculas = new ArrayList();
		try {
			String hql = "from Matriculas";
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			Query q = session.createQuery(hql);
			matriculas = (ArrayList) q.list();
			session.getTransaction().commit();
		} catch (HibernateException he) {
			he.printStackTrace();
		}
		return matriculas;
	}

	@Override
	public void update(Alumnos alumno) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(alumno);
			session.getTransaction().commit();
			f.info("Alumno " + alumno.getNombrea() + " actualizado correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
		}
	}
	
	@Override
	public void update(Cursos curso) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(curso);
			session.getTransaction().commit();
			f.info("Curso " + curso.getNombrec() + " actualizado correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
		}
	}
	
	@Override
	public void update(Matriculas matricula) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(matricula);
			session.getTransaction().commit();
			f.info("Matrícula actualizada correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
		}
	}

	@Override
	public void delete(Alumnos alumno) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(alumno);
			session.getTransaction().commit();
			f.info("Alumno " + alumno.getNombrea() + " borrado correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
			f.error("El alumno no ha podido borrarse. Consultar si tiene matrículas asociadas, y eliminar primero la matrícula. \n"
					+ "Error: " + he.getMessage());
		}
	}

	@Override
	public void delete(Cursos curso) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(curso);
			session.getTransaction().commit();
			f.info("Curso " + curso.getNombrec() + " borrado correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
			f.error("El curso no ha podido borrarse. Consultar si tiene matrículas asociadas, y eliminar primero la matrícula. \n"
					+ "Error: " + he.getMessage());
		}
	}
	
	@Override
	public void delete(Matriculas matricula) {
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(matricula);
			session.getTransaction().commit();
			f.info("Matricula borrada correctamente");
		} catch (HibernateException he) {
			he.printStackTrace();
			f.error("La matrícula no ha podido borrarse. \n"
					+ "Error: " + he.getMessage());
		}
	}
	
	@Override
	public Alumnos search(Alumnos alumno) {
		alumnos = new ArrayList();
		String hql = "FROM Alumnos A WHERE A.id = :alumno_id";
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Query q = session.createQuery(hql);
		q.setParameter("alumno_id",alumno.getIda());
		alumnos = (ArrayList) q.list();
		session.getTransaction().commit();
        Iterator iterator = alumnos.iterator();
        while (iterator.hasNext()) {
            Alumnos a = (Alumnos) iterator.next();
            if (Objects.equals(alumno.getIda(), a.getIda())) {
                alumno = a;
            }
        }
		return alumno;
	}

	@Override
	public Cursos search(Cursos curso) {
		cursos = new ArrayList();
		String hql = "FROM Cursos C WHERE C.id = :curso_id";
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Query q = session.createQuery(hql);
		q.setParameter("curso_id",curso.getIdc());
		alumnos = (ArrayList) q.list();
		session.getTransaction().commit();
        Iterator iterator = alumnos.iterator();
        while (iterator.hasNext()) {
            Cursos c = (Cursos) iterator.next();
            if (Objects.equals(curso.getIdc(), c.getIdc())) {
                curso = c;
            }
        }
		return curso;
	}

	@Override
	public Matriculas search(Matriculas matricula) {
		matriculas = new ArrayList();
		String hql = "FROM Matriculas M WHERE M.id = :matricula_id";
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Query q = session.createQuery(hql);
		q.setParameter("matricula_id",matricula.getIdm());
		matriculas = (ArrayList) q.list();
		session.getTransaction().commit();
        Iterator iterator = matriculas.iterator();
        while (iterator.hasNext()) {
            Matriculas m = (Matriculas) iterator.next();
            if (Objects.equals(matricula.getIdm(), m.getIdm())) {
                matricula = m;
            }
        }
		return matricula;
	}
}