package org.ceedcv.ceed77prgpro10.modelo;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.ceedcv.ceed77prgpro10.vista.Funciones;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class ModeloDb4o implements IModelo {
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private final String bbddoo = "bbddoo.db4o";
	private final File existe = new File(bbddoo);
	private ObjectContainer bbdd;
	private ObjectSet os;
	private final Funciones f = new Funciones();
	private int idA = 1, idC = 1, idM = 1;
	private ArrayList alumnos, cursos, matriculas;
	private Alumnos alumno;
	private Cursos curso;
	private Matriculas matricula;

	public ModeloDb4o() {
		if (!existe.exists()) {
			f.error("No existe la base de datos orientada a objetos. \n"
					+ "Deberás instalarla previamente para usar el programa.");
		}
	}

	@Override
	public void conectarBd() {
		bbdd = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), bbddoo);
		System.out.println("Base de datos orientada a objetos conectada.");
	}

	@Override
	public void desconectarBd() {
		bbdd.close();
		System.out.println("Base de datos orientada a objetos desconectada.");
	}

	@Override
	public void crearBd() {
		try {
			Alumnos alumno1 = new Alumnos(0, "Juan", "44522840-S", 25, "juan@gmail.com", null);
			Alumnos alumno2 = new Alumnos(1, "Luis", "44522840-S", 36, "luis@gmail.com", null);
			Cursos curso1 = new Cursos(0, "Matemáticas", 40, null);
			Cursos curso2 = new Cursos(1, "Lengua", 80, null);
			create(alumno1);
			create(alumno2);
			create(curso1);
			create(curso2);
//			Matriculas matricula1 = new Matriculas(0, alumno1, curso1, sdf.parse("2016-03-12"));
//			Matriculas matricula2 = new Matriculas(1, alumno2, curso2, sdf.parse("2016-03-26"));
//			create(matricula1);
//			create(matricula2);
			f.info("Base de datos orientada a objetos creada correctamente.");
		} catch (Exception e) {
			e.printStackTrace();
			f.error("Ha ocurrido un error durante la instalación."
					+ "Error: " + e.getMessage());
		}
	}

	@Override
	public void eliminarBd() {
		try {
			idA = 1;
			idC = 1;
			idM = 1;
			existe.delete();
			f.info("Base de datos " + bbddoo + "eliminada correctamente.");
		} catch (Exception e) {
			e.printStackTrace();
			f.error("Ha ocurrido un error durante la desinstalación."
					+ "Error: " + e.getMessage());
		}
	}

	@Override
	public void crearTablas() {
		//No procede
	}

	@Override
	public void crearDatos() {
		//No procede
	}

	//Funciones CRUD alumno, curso y matrícula
	@Override
	public void create(Alumnos alumno) {
		if (existe.exists()) {
			idA = idAlumno();
		}
		conectarBd();
		alumno.setIda(idA);
		idA++;
		bbdd.store(alumno);
		desconectarBd();
		System.out.println("Alumno " + alumno.getNombrea() + " creado correctamente.");
		f.info("Alumno " + alumno.getNombrea() + " creado correctamente.");
	}

	@Override
	public void create(Cursos curso) {
		if (existe.exists()) {
			idC = idCurso();
		}
		conectarBd();
		curso.setIdc(idC);
		idC++;
		bbdd.store(curso);
		desconectarBd();
		System.out.println("Curso " + curso.getNombrec() + " creado correctamente.");
		f.info("Curso " + curso.getNombrec() + " creado correctamente.");
	}

	@Override
	public void create(Matriculas matricula) {
		if (existe.exists()) {
			idM = idMatricula();
		}
		conectarBd();
		matricula.setIdm(idM);
		Alumnos a = objetoAlumno(matricula);
		Cursos c = objetoCurso(matricula);
		if (a != null) {
			matricula.setAlumnos(a);
		}
		if (c != null) {
			matricula.setCursos(c);
		}
		idM++;
		bbdd.store(matricula);
		desconectarBd();
		System.out.println("Matrícula " + matricula.getAlumnos().getNombrea() + " - " 
				+ matricula.getCursos().getNombrec() + " creada correctamente.");
		f.info("Matrícula " + matricula.getAlumnos().getNombrea() + " - " 
				+ matricula.getCursos().getNombrec() + " creada correctamente.");
	}

	@Override
	public ArrayList readAlumno() {
		alumnos = new ArrayList();
		alumno = new Alumnos(0, null, null, 0, null);
		conectarBd();
		os = bbdd.queryByExample(alumno);
		while (os.hasNext()) {
			Alumno a = (Alumno) os.next();
			alumnos.add(a);
		}
		desconectarBd();
		return alumnos;
	}

	@Override
	public ArrayList readCurso() {
		cursos = new ArrayList();
		curso = new Cursos();
		conectarBd();
		os = null;
		os = bbdd.queryByExample(curso);
		while (os.hasNext()) {
			curso = (Cursos) os.next();
			cursos.add(curso);
		}
		desconectarBd();
		return cursos;
	}

	@Override
	public ArrayList readMatricula() {
		matriculas = new ArrayList();
		matricula = new Matriculas();
		conectarBd();
		os = null;
		os = bbdd.queryByExample(matricula);
		while (os.hasNext()) {
			matricula = (Matriculas) os.next();
			matriculas.add(matricula);
		}
		desconectarBd();
		return matriculas;
	}

	@Override
	public void update(Alumnos alumno) {
		conectarBd();
		Alumnos al = new Alumnos(alumno.getIda(), null, null, 0, null);
		os = null;
		os = bbdd.queryByExample(al);
		if (os.hasNext()) {
			Alumnos a = (Alumnos) os.next();
			a.setNombrea(alumno.getNombrea());
			a.setDni(alumno.getDni());
			a.setEdad(alumno.getEdad());
			a.setEmail(alumno.getEmail());
			bbdd.store(a);
			System.out.println("Alumno " + a.getNombrea() + " actualizado correctamente.");
			f.info("Alumno " + a.getNombrea() + " actualizado correctamente.");
		}
		desconectarBd();
	}

	@Override
	public void update(Cursos curso) {
		conectarBd();
		Cursos cur = new Cursos(curso.getIdc(), null, 0);
		os = null;
		os = bbdd.queryByExample(cur);
		if (os.hasNext()) {
			Cursos c = (Cursos) os.next();
			c.setNombrec(curso.getNombrec());
			c.setHoras(curso.getHoras());
			bbdd.store(c);
			System.out.println("Curso " + c.getNombrec() + " actualizado correctamente.");
			f.info("Curso " + c.getNombrec() + " actualizado correctamente.");
		}
		desconectarBd();
	}

	@Override
	public void update(Matriculas matricula) {
		conectarBd();
		Matricula mat = new Matricula(matricula.getIdm(), null, null, null);
		os = null;
		os = bbdd.queryByExample(mat);
		if (os.hasNext()) {
			Matriculas m = (Matriculas) os.next();
			m.setFecha(matricula.getFecha());
			Alumnos a = objetoAlumno(matricula);
			Cursos c = objetoCurso(matricula);
			if (a != null) {
				m.setAlumnos(a);
			}
			if (c != null) {
				m.setCursos(c);
			}
			bbdd.store(m);
			System.out.println("Matrícula " + matricula.getAlumnos().getNombrea() + " - " + matricula.getCursos().getNombrec() + " actualizada correctamente.");
			f.info("Matrícula " + matricula.getAlumnos().getNombrea() + " - " + matricula.getCursos().getNombrec() + " actualizada correctamente.");
		}
		desconectarBd();
	}

	@Override
	public void delete(Alumnos alumno) {
		conectarBd();
		Alumnos al = new Alumnos(alumno.getIda(), null, null, 0, null);
		os = null;
		os = bbdd.queryByExample(al);
		if (os.hasNext()) {
			Alumnos a = (Alumnos) os.next();
			bbdd.delete(a);
			System.out.println("Alumno " + a.getNombrea() + "borrado correctamente.");
			f.info("Alumno " + a.getNombrea() + " borrado correctamente.");
		}
		desconectarBd();
	}

	@Override
	public void delete(Cursos curso) {
		conectarBd();
		Cursos cur = new Cursos(curso.getIdc(), null, 0);
		os = null;
		os = bbdd.queryByExample(cur);
		if (os.hasNext()) {
			Cursos c = (Cursos) os.next();
			bbdd.delete(c);
			System.out.println("Curso " + c.getNombrec() + "borrado correctamente.");
			f.info("Curso " + c.getNombrec() + " borrado correctamente.");
		}
		desconectarBd();
	}

	@Override
	public void delete(Matriculas matricula) {
		conectarBd();
		Matriculas mat = new Matriculas(matricula.getIdm(), null, null, null);
		os = null;
		os = bbdd.queryByExample(mat);
		if (os.hasNext()) {
			Matriculas m = (Matriculas) os.next();
			bbdd.delete(m);
			System.out.println("Matrícula " + matricula.getAlumnos().getNombrea() + " - " 
					+ matricula.getCursos().getNombrec() + " borrada correctamente.");
			f.info("Matrícula " + matricula.getAlumnos().getNombrea() + " - " 
					+ matricula.getCursos().getNombrec() + " borrada correctamente.");
		}
		desconectarBd();
	}

	@Override
	public Alumnos search(Alumnos alumno) {
		conectarBd();
		Alumnos al = new Alumnos(alumno.getIda(), null, null, 0, null);
		alumno = null;
		os = null;
		os = bbdd.queryByExample(al);
		if (os.hasNext()) {
			Alumnos a = (Alumnos) os.next();
			alumno = a;
		}
		if (alumno == null) {
			f.error("El ID proporcionado no corresponde a ningún alumno.");
		}
		desconectarBd();
		return alumno;
	}

	@Override
	public Cursos search(Cursos curso) {
		conectarBd();
		Cursos cur = new Cursos(curso.getIdc(), null, 0);
		curso = null;
		os = null;
		os = bbdd.queryByExample(cur);
		if (os.hasNext()) {
			Cursos c = (Cursos) os.next();
			curso = c;
		}
		if (curso == null) {
			f.error("El ID proporcionado no corresponde a ningún curso.");
		}
		desconectarBd();
		return curso;
	}

	@Override
	public Matriculas search(Matriculas matricula) {
		conectarBd();
		Matriculas mat = new Matriculas(matricula.getIdm(), null, null, null);
		matricula = null;
		os = null;
		os = bbdd.queryByExample(mat);
		if (os.hasNext()) {
			Matriculas m = (Matriculas) os.next();
			matricula = m;
		}
		if (matricula == null) {
			f.error("El ID proporcionado no corresponde a ninguna matricula.");
		}
		desconectarBd();
		return matricula;
	}

	/* Otras funciones */
	//Calcular la ID actual
	private int idAlumno() {
		int idActual = 0;
		alumno = new Alumnos(0, null, null, 0, null);
		conectarBd();
		os = null;
		os = bbdd.queryByExample(alumno);
		while (os.hasNext()) {
			Alumnos a = (Alumnos) os.next();
			int id = a.getIda();
			if (id > idActual) {
				idActual = id;
			}
		}
		desconectarBd();
		return idActual + 1;
	}

	private int idCurso() {
		int idActual = 0;
		curso = new Cursos(0, null, 0);
		conectarBd();
		os = null;
		os = bbdd.queryByExample(curso);
		while (os.hasNext()) {
			Cursos c = (Cursos) os.next();
			int id = c.getIdc();
			if (id > idActual) {
				idActual = id;
			}
		}
		desconectarBd();
		return idActual + 1;
	}

	private int idMatricula() {
		int idActual = 0;
		matricula = new Matriculas(0, null, null, null);
		conectarBd();
		os = null;
		os = bbdd.queryByExample(matricula);
		while (os.hasNext()) {
			Matriculas m = (Matriculas) os.next();
			int id = m.getIdm();
			if (id > idActual) {
				idActual = id;
			}
		}
		desconectarBd();
		return idActual + 1;
	}

	//Obtener el objeto exacto
	private Alumnos objetoAlumno(Matriculas matricula) {
		alumno = null;
		Alumnos a = new Alumnos(matricula.getAlumnos().getIda(), null, null, 0, null);
		os = null;
		os = bbdd.queryByExample(a);
		alumno = (Alumnos) os.next();
		return alumno;
	}

	private Cursos objetoCurso(Matriculas matricula) {
		curso = null;
		Cursos c = new Cursos(matricula.getCursos().getIdc(), null, 0);
		os = null;
		os = bbdd.queryByExample(c);
		curso = (Cursos) os.next();
		return curso;
	}
}