package org.ceedcv.ceed77prgpro10.modelo;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class Curso {
	//Variables de la clase
	private int idCurso;
	private String nombre;
	private int horas;

	//Constructores de la clase
	public Curso() {

	}

	public Curso(int idC, String n, int h) {
		idCurso = idC;
		nombre = n;
		horas = h;
	}

	//Métodos de la clase
	//Getters
	public int getIdCurso() {
		return idCurso;
	}

	public String getNombre() {
		return nombre;
	}

	public int getHoras() {
		return horas;
	}

	//Setters
	public void setIdCurso(int idC) {
		idCurso = idC;
	}

	public void setNombre(String n) {
		nombre = n;
	}

	public void setHoras(int h) {
		horas = h;
	}
}
