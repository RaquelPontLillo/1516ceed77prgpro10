package org.ceedcv.ceed77prgpro10.modelo;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class Alumno extends Persona {

	//Variables de clase
	private int edad;
	private String email;

	//Constructores de la clase
	public Alumno() {

	}

	public Alumno(int idP, String n, String d, int e, String m) {
		super(idP, n, d);
		this.edad = e;
		this.email = m;
	}

	//Métodos de la clase
	//Getters
	public int getEdad() {
		return edad;
	}

	public String getEmail() {
		return email;
	}

	//Setters
	public void setEdad(int e) {
		edad = e;
	}

	public void setEmail(String m) {
		email = m;
	}
}
