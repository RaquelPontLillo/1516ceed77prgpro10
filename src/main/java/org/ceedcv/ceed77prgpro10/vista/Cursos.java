/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed77prgpro10.vista;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Ukyo
 */
@Entity
@Table(name = "cursos", catalog = "1516ceed77prg", schema = "")
@NamedQueries({
	@NamedQuery(name = "Cursos.findAll", query = "SELECT c FROM Cursos c"),
	@NamedQuery(name = "Cursos.findByIdc", query = "SELECT c FROM Cursos c WHERE c.idc = :idc"),
	@NamedQuery(name = "Cursos.findByNombrec", query = "SELECT c FROM Cursos c WHERE c.nombrec = :nombrec"),
	@NamedQuery(name = "Cursos.findByHoras", query = "SELECT c FROM Cursos c WHERE c.horas = :horas")})
public class Cursos implements Serializable {
	@Transient
	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idc")
	private Integer idc;
	@Basic(optional = false)
    @Column(name = "nombrec")
	private String nombrec;
	@Column(name = "horas")
	private Integer horas;

	public Cursos() {
	}

	public Cursos(Integer idc) {
		this.idc = idc;
	}

	public Cursos(Integer idc, String nombrec) {
		this.idc = idc;
		this.nombrec = nombrec;
	}

	public Integer getIdc() {
		return idc;
	}

	public void setIdc(Integer idc) {
		Integer oldIdc = this.idc;
		this.idc = idc;
		changeSupport.firePropertyChange("idc", oldIdc, idc);
	}

	public String getNombrec() {
		return nombrec;
	}

	public void setNombrec(String nombrec) {
		String oldNombrec = this.nombrec;
		this.nombrec = nombrec;
		changeSupport.firePropertyChange("nombrec", oldNombrec, nombrec);
	}

	public Integer getHoras() {
		return horas;
	}

	public void setHoras(Integer horas) {
		Integer oldHoras = this.horas;
		this.horas = horas;
		changeSupport.firePropertyChange("horas", oldHoras, horas);
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idc != null ? idc.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Cursos)) {
			return false;
		}
		Cursos other = (Cursos) object;
		if ((this.idc == null && other.idc != null) || (this.idc != null && !this.idc.equals(other.idc))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "org.ceedcv.ceed77prgpro10.vista.Cursos[ idc=" + idc + " ]";
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
	}
	
}
