/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed77prgpro10.vista;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Ukyo
 */
@Entity
@Table(name = "alumnos", catalog = "1516ceed77prg", schema = "")
@NamedQueries({
	@NamedQuery(name = "Alumnos.findAll", query = "SELECT a FROM Alumnos a"),
	@NamedQuery(name = "Alumnos.findByIda", query = "SELECT a FROM Alumnos a WHERE a.ida = :ida"),
	@NamedQuery(name = "Alumnos.findByNombrea", query = "SELECT a FROM Alumnos a WHERE a.nombrea = :nombrea"),
	@NamedQuery(name = "Alumnos.findByDni", query = "SELECT a FROM Alumnos a WHERE a.dni = :dni"),
	@NamedQuery(name = "Alumnos.findByEdad", query = "SELECT a FROM Alumnos a WHERE a.edad = :edad"),
	@NamedQuery(name = "Alumnos.findByEmail", query = "SELECT a FROM Alumnos a WHERE a.email = :email")})
public class Alumnos implements Serializable {
	@Transient
	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ida")
	private Integer ida;
	@Column(name = "nombrea")
	private String nombrea;
	@Column(name = "dni")
	private String dni;
	@Column(name = "edad")
	private Integer edad;
	@Column(name = "email")
	private String email;

	public Alumnos() {
	}

	public Alumnos(Integer ida) {
		this.ida = ida;
	}

	public Integer getIda() {
		return ida;
	}

	public void setIda(Integer ida) {
		Integer oldIda = this.ida;
		this.ida = ida;
		changeSupport.firePropertyChange("ida", oldIda, ida);
	}

	public String getNombrea() {
		return nombrea;
	}

	public void setNombrea(String nombrea) {
		String oldNombrea = this.nombrea;
		this.nombrea = nombrea;
		changeSupport.firePropertyChange("nombrea", oldNombrea, nombrea);
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		String oldDni = this.dni;
		this.dni = dni;
		changeSupport.firePropertyChange("dni", oldDni, dni);
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		Integer oldEdad = this.edad;
		this.edad = edad;
		changeSupport.firePropertyChange("edad", oldEdad, edad);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		String oldEmail = this.email;
		this.email = email;
		changeSupport.firePropertyChange("email", oldEmail, email);
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (ida != null ? ida.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Alumnos)) {
			return false;
		}
		Alumnos other = (Alumnos) object;
		if ((this.ida == null && other.ida != null) || (this.ida != null && !this.ida.equals(other.ida))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "org.ceedcv.ceed77prgpro10.vista.Alumnos[ ida=" + ida + " ]";
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.removePropertyChangeListener(listener);
	}
	
}
