package org.ceedcv.ceed77prgpro10.vista;

import javax.swing.*;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Funciones {
    public JOptionPane error(String s) {
        JOptionPane error = new JOptionPane();
        ImageIcon icon = new ImageIcon(getClass().getResource("/imagenes/error.png"));
        error.showMessageDialog(null, s, "Error", 0, icon);
        return error;
    }
    
    public JOptionPane info(String s) {
        JOptionPane info = new JOptionPane();
        ImageIcon icon = new ImageIcon(getClass().getResource("/imagenes/informacion.png"));
        info.showMessageDialog(null, s, "Información", 0, icon);
        return info;
    }
    
    public JOptionPane pregunta(String s) {
        JOptionPane info = new JOptionPane();
        ImageIcon icon = new ImageIcon(getClass().getResource("/imagenes/pregunta.png"));
        info.showConfirmDialog(null, s, "Atención", JOptionPane.YES_NO_CANCEL_OPTION, 0, icon);
        return info;
    }
}                     