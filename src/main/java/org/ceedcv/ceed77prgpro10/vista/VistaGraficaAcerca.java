package org.ceedcv.ceed77prgpro10.vista;

import javax.swing.*;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VistaGraficaAcerca extends JInternalFrame {        
        private static VistaGraficaAcerca instancia;
        private JLabel logo = new JLabel();
        private JLabel nombre = new JLabel("KANDELSOFT INC.");
        private JLabel email = new JLabel("contact@kandelsoft.com");
        private JLabel anyo = new JLabel("2015 - 2016");

    private VistaGraficaAcerca() {
    //Llamamos al constructor de la clase JFrame
        super("Acerca de...");
        
        //Configuramos el comportamiento del aspa y aspecto general de la GUI
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.err.println("No se ha podido configurar el Look and Feel escogido");
            e.printStackTrace();
        }
        setResizable(false);
        setMaximizable(true);
        setIconifiable(true);
        setClosable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/kandelsoft.png")));
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nombre, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                    .addComponent(email, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(anyo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(logo)
                .addContainerGap(23, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(logo)
                .addContainerGap(27, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(nombre)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(email)
                .addGap(35, 35, 35)
                .addComponent(anyo)
                .addGap(47, 47, 47))
        );
    }
    
    //Getters
    public static VistaGraficaAcerca getInstance() {
        if (instancia == null) {
            instancia = new VistaGraficaAcerca();
        }
        return instancia;
    }   
}