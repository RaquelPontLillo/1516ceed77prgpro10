package org.ceedcv.ceed77prgpro10.vista;

import java.awt.EventQueue;
import java.beans.Beans;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.RollbackException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Ukyo
 */
public class VistaTablaAlumnos extends JPanel {

	private javax.swing.JButton deleteButton;
	private javax.swing.JTextField dniField;
	private javax.swing.JLabel dniLabel;
	private javax.swing.JTextField edadField;
	private javax.swing.JLabel edadLabel;
	private javax.swing.JTextField emailField;
	private javax.swing.JLabel emailLabel;
	private javax.persistence.EntityManager entityManager;
	private javax.swing.JTextField idaField;
	private javax.swing.JLabel idaLabel;
	private java.util.List<org.ceedcv.ceed77prgpro10.vista.Alumnos> list;
	private javax.swing.JScrollPane masterScrollPane;
	private javax.swing.JTable masterTable;
	private javax.swing.JButton newButton;
	private javax.swing.JTextField nombreaField;
	private javax.swing.JLabel nombreaLabel;
	private javax.persistence.Query query;
	private javax.swing.JButton refreshButton;
	private javax.swing.JButton saveButton;
	private org.jdesktop.beansbinding.BindingGroup bindingGroup;

	public VistaTablaAlumnos() {
		initComponents();
		if (!Beans.isDesignTime()) {
			entityManager.getTransaction().begin();
		}
	}

	private void initComponents() {
		bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

		entityManager = java.beans.Beans.isDesignTime() ? null : javax.persistence.Persistence.createEntityManagerFactory("1516ceed77prg?zeroDateTimeBehavior=convertToNullPU").createEntityManager();
		query = java.beans.Beans.isDesignTime() ? null : entityManager.createQuery("SELECT a FROM Alumnos a");
		list = java.beans.Beans.isDesignTime() ? java.util.Collections.emptyList() : org.jdesktop.observablecollections.ObservableCollections.observableList(query.getResultList());
		masterScrollPane = new javax.swing.JScrollPane();
		masterTable = new javax.swing.JTable();
		idaLabel = new javax.swing.JLabel();
		nombreaLabel = new javax.swing.JLabel();
		dniLabel = new javax.swing.JLabel();
		edadLabel = new javax.swing.JLabel();
		emailLabel = new javax.swing.JLabel();
		idaField = new javax.swing.JTextField();
		nombreaField = new javax.swing.JTextField();
		dniField = new javax.swing.JTextField();
		edadField = new javax.swing.JTextField();
		emailField = new javax.swing.JTextField();
		saveButton = new javax.swing.JButton();
		refreshButton = new javax.swing.JButton();
		newButton = new javax.swing.JButton();
		deleteButton = new javax.swing.JButton();

		FormListener formListener = new FormListener();

		org.jdesktop.swingbinding.JTableBinding jTableBinding = org.jdesktop.swingbinding.SwingBindings.createJTableBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, list, masterTable);
		org.jdesktop.swingbinding.JTableBinding.ColumnBinding columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${ida}"));
		columnBinding.setColumnName("ID");
		columnBinding.setColumnClass(Integer.class);
		columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${nombrea}"));
		columnBinding.setColumnName("Nombre");
		columnBinding.setColumnClass(String.class);
		columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${dni}"));
		columnBinding.setColumnName("DNI/NIE");
		columnBinding.setColumnClass(String.class);
		columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${edad}"));
		columnBinding.setColumnName("Edad");
		columnBinding.setColumnClass(Integer.class);
		columnBinding = jTableBinding.addColumnBinding(org.jdesktop.beansbinding.ELProperty.create("${email}"));
		columnBinding.setColumnName("Email");
		columnBinding.setColumnClass(String.class);
		bindingGroup.addBinding(jTableBinding);

		masterScrollPane.setViewportView(masterTable);

		idaLabel.setText("ID:");

		nombreaLabel.setText("Nombre:");

		dniLabel.setText("DNI/NIE:");

		edadLabel.setText("Edad:");

		emailLabel.setText("Email:");

		org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.ida}"), idaField, org.jdesktop.beansbinding.BeanProperty.create("text"));
		binding.setSourceUnreadableValue("null");
		bindingGroup.addBinding(binding);
		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), idaField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
		bindingGroup.addBinding(binding);

		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.nombrea}"), nombreaField, org.jdesktop.beansbinding.BeanProperty.create("text"));
		binding.setSourceUnreadableValue("null");
		bindingGroup.addBinding(binding);
		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), nombreaField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
		bindingGroup.addBinding(binding);

		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.dni}"), dniField, org.jdesktop.beansbinding.BeanProperty.create("text"));
		binding.setSourceUnreadableValue("null");
		bindingGroup.addBinding(binding);
		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), dniField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
		bindingGroup.addBinding(binding);

		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.edad}"), edadField, org.jdesktop.beansbinding.BeanProperty.create("text"));
		binding.setSourceUnreadableValue("null");
		bindingGroup.addBinding(binding);
		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), edadField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
		bindingGroup.addBinding(binding);

		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement.email}"), emailField, org.jdesktop.beansbinding.BeanProperty.create("text"));
		binding.setSourceUnreadableValue("null");
		bindingGroup.addBinding(binding);
		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), emailField, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
		bindingGroup.addBinding(binding);

		saveButton.setIcon(new ImageIcon(getClass().getResource("/imagenes/guardar.png")));
		saveButton.addActionListener(formListener);

		refreshButton.setIcon(new ImageIcon(getClass().getResource("/imagenes/update.png")));
		refreshButton.addActionListener(formListener);

		newButton.setIcon(new ImageIcon(getClass().getResource("/imagenes/create.png")));
		newButton.addActionListener(formListener);

		deleteButton.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));

		binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ, masterTable, org.jdesktop.beansbinding.ELProperty.create("${selectedElement != null}"), deleteButton, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
		bindingGroup.addBinding(binding);

		deleteButton.addActionListener(formListener);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
			layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(layout.createSequentialGroup()
				.addContainerGap()
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
									.addComponent(idaLabel)
									.addComponent(nombreaLabel)
									.addComponent(dniLabel)
									.addComponent(edadLabel)
									.addComponent(emailLabel))
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
									.addComponent(idaField, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
									.addComponent(nombreaField, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
									.addComponent(dniField, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
									.addComponent(edadField, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
									.addComponent(emailField, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)))
					.addComponent(masterScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 543, Short.MAX_VALUE)
					.addGroup(layout.createSequentialGroup()
							.addComponent(newButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
							.addComponent(refreshButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
							.addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
							.addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
			.addContainerGap())
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(masterScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(idaLabel)
								.addComponent(idaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(nombreaLabel)
								.addComponent(nombreaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(dniLabel)
								.addComponent(dniField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(edadLabel)
								.addComponent(edadField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(emailLabel)
								.addComponent(emailField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(saveButton)
										.addComponent(refreshButton)
										.addComponent(deleteButton)
										.addComponent(newButton)))
						.addContainerGap())
		);

		bindingGroup.bind();
	}

	//Eventos de la interfaz
	private class FormListener implements java.awt.event.ActionListener {

		FormListener() {
		}

		@Override
		public void actionPerformed(java.awt.event.ActionEvent evt) {
			if (evt.getSource() == saveButton) {
				VistaTablaAlumnos.this.saveButtonActionPerformed(evt);
			} else if (evt.getSource() == refreshButton) {
				VistaTablaAlumnos.this.refreshButtonActionPerformed(evt);
			} else if (evt.getSource() == newButton) {
				VistaTablaAlumnos.this.newButtonActionPerformed(evt);
			} else if (evt.getSource() == deleteButton) {
				VistaTablaAlumnos.this.deleteButtonActionPerformed(evt);
			}
		}
	}

	private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {
		entityManager.getTransaction().rollback();
		entityManager.getTransaction().begin();
		java.util.Collection data = query.getResultList();
		for (Object entity : data) {
			entityManager.refresh(entity);
		}
		list.clear();
		list.addAll(data);
	}

	private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {
		int[] selected = masterTable.getSelectedRows();
		List<org.ceedcv.ceed77prgpro10.vista.Alumnos> toRemove = new ArrayList<>(selected.length);
		for (int idx = 0; idx < selected.length; idx++) {
			org.ceedcv.ceed77prgpro10.vista.Alumnos a = list.get(masterTable.convertRowIndexToModel(selected[idx]));
			toRemove.add(a);
			entityManager.remove(a);
		}
		list.removeAll(toRemove);
	}

	private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {
		org.ceedcv.ceed77prgpro10.vista.Alumnos a = new org.ceedcv.ceed77prgpro10.vista.Alumnos();
		entityManager.persist(a);
		list.add(a);
		int row = list.size() - 1;
		masterTable.setRowSelectionInterval(row, row);
		masterTable.scrollRectToVisible(masterTable.getCellRect(row, 0, true));
	}

	private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {
		try {
			entityManager.getTransaction().commit();
			entityManager.getTransaction().begin();
		} catch (RollbackException rex) {
			rex.printStackTrace();
			entityManager.getTransaction().begin();
			List<org.ceedcv.ceed77prgpro10.vista.Alumnos> merged = new ArrayList<>(list.size());
			for (org.ceedcv.ceed77prgpro10.vista.Alumnos a : list) {
				merged.add(entityManager.merge(a));
			}
			list.clear();
			list.addAll(merged);
		}
	}

	public static void main(String[] args) {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(VistaTablaAlumnos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		/* Create and display the form */
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				JFrame frame = new JFrame();
				frame.setContentPane(new VistaTablaAlumnos());
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.pack();
				frame.setVisible(true);
			}
		});
	}
}
