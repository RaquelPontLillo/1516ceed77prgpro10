package org.ceedcv.ceed77prgpro10.vista;

import javax.swing.*;

public class VistaLogin extends JFrame {
	private JButton jButton1 = new JButton();
    private JLabel jLabel2 = new JLabel();
    private JPasswordField jPasswordField1 = new JPasswordField();
    private JTextField jTextField1 = new JTextField();
	
	public VistaLogin() {
		//Llamamos al constructor de la clase JFrame
		super("Control de acceso");
		try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.err.println("No se ha podido configurar el Look and Feel escogido");
            e.printStackTrace();
        }
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
		setLocationRelativeTo(null);

        jPasswordField1.setText("pass");
        jTextField1.setText("root");
        jLabel2.setIcon(new ImageIcon(getClass().getResource("/imagenes/bloqueo.png")));
        jButton1.setIcon(new ImageIcon(getClass().getResource("/imagenes/llave.png")));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPasswordField1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(105, 105, 105)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING)))
                .addContainerGap(121, Short.MAX_VALUE))
        );
        pack();
		
		//Hacemos visible la GUI
		setVisible(true);
	}

	/**
	 * @return the jButton1
	 */
	public JButton getjButton1() {
		return jButton1;
	}

	/**
	 * @return the jPasswordField1
	 */
	public JPasswordField getjPasswordField1() {
		return jPasswordField1;
	}

	/**
	 * @return the jTextField1
	 */
	public JTextField getjTextField1() {
		return jTextField1;
	}
}