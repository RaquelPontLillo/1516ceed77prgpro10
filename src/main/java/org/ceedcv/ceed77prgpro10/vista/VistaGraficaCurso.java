package org.ceedcv.ceed77prgpro10.vista;

import javax.swing.*;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VistaGraficaCurso extends JInternalFrame {
    private static VistaGraficaCurso instancia;
    
    //Definimos los botones de la GUI
    private JButton create = new JButton();
    private JButton read = new JButton();
    private JButton update = new JButton();
    private JButton delete = new JButton();
    private JButton search = new JButton();
    private JButton guardar = new JButton();
    private JButton cancelar = new JButton();
    private JButton salir = new JButton();
    private JButton siguiente = new JButton();
    private JButton anterior = new JButton();
    private JButton primero = new JButton();
    private JButton ultimo = new JButton();
    
    //Definimos las etiquetas de la GUI
    private JLabel id = new JLabel("ID: ", JLabel.RIGHT);
    private JLabel nombre = new JLabel("Nombre: ",  JLabel.RIGHT);
    private JLabel horas = new JLabel("Horas: ", JLabel.RIGHT);
    private JLabel informacion = new JLabel("MENÚ CURSO");
    private JLabel logoCurso = new JLabel();
    
    //Definimos los campos de texto de la GUI
    private JTextField tid = new JTextField();
    private JTextField tnombre = new JTextField();
    private JTextField thoras = new JTextField();
    
    //Definimos los paneles de la GUI
    private JPanel panel = new JPanel();
    
    private VistaGraficaCurso() {
        //Llamamos al constructor de la clase JFrame
        super("Menú curso");
        
        //Configuramos el comportamiento del aspa y aspecto general de la GUI
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.err.println("No se ha podido configurar el Look and Feel escogido");
            e.printStackTrace();
        }
        setResizable(false);
        setSize(690,344);
        setMaximizable(true);
        setIconifiable(true);
        setClosable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        //Configuramos los distintos componentes de la GUI
        //CRUD
        create.setToolTipText("Añadir nuevo");
        create.setIcon(new ImageIcon(getClass().getResource("/imagenes/create.png")));
        read.setToolTipText("Leer datos");
        read.setIcon(new ImageIcon(getClass().getResource("/imagenes/read.png")));
        update.setToolTipText("Actualizar información");
        update.setIcon(new ImageIcon(getClass().getResource("/imagenes/update.png")));
        update.setEnabled(false);
        delete.setToolTipText("Borrar información");
        delete.setIcon(new ImageIcon(getClass().getResource("/imagenes/delete.png")));
        delete.setEnabled(false);
        search.setToolTipText("Buscar por ID");
        search.setIcon(new ImageIcon(getClass().getResource("/imagenes/search.png")));
        //Navegación
        primero.setToolTipText("Volver al principio");
        primero.setIcon(new ImageIcon(getClass().getResource("/imagenes/primero.png")));
        primero.setEnabled(false);
        anterior.setToolTipText("Anterior");
        anterior.setIcon(new ImageIcon(getClass().getResource("/imagenes/anterior.png")));
        anterior.setEnabled(false);
        siguiente.setToolTipText("Siguiente");
        siguiente.setIcon(new ImageIcon(getClass().getResource("/imagenes/siguiente.png")));
        siguiente.setEnabled(false);
        ultimo.setToolTipText("Ir al último");
        ultimo.setIcon(new ImageIcon(getClass().getResource("/imagenes/ultimo.png")));
        ultimo.setEnabled(false);
        //Guardar, cancelar, salir
        guardar.setToolTipText("Guardar datos");
        guardar.setIcon(new ImageIcon(getClass().getResource("/imagenes/guardar.png")));
        guardar.setEnabled(false);
        cancelar.setToolTipText("Cancelar");
        cancelar.setIcon(new ImageIcon(getClass().getResource("/imagenes/cancelar.png")));
        cancelar.setEnabled(false);
        salir.setToolTipText("Salir del menú");
        salir.setIcon(new ImageIcon(getClass().getResource("/imagenes/salir.png")));
        //Logo
        logoCurso.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo_curso.png")));
        //Datos alumno
        tid.setEnabled(false);
        tnombre.setEnabled(false);
        thoras.setEnabled(false);

        //Definimos el aspecto para nuestro diseño y añadimos los botones y paneles
        panel.setBorder(BorderFactory.createTitledBorder("Datos del curso"));
        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(id)
                        .addGap(52, 52, 52)
                        .addComponent(tid, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nombre)
                            .addComponent(horas))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(thoras, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(id)
                    .addComponent(tid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre)
                    .addComponent(tnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(horas)
                    .addComponent(thoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26))
        );
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(227, Short.MAX_VALUE)
                        .addComponent(primero, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(anterior, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(siguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ultimo, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(110, 110, 110)
                        .addComponent(guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(salir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(create, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(read, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(update, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(delete, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(45, 45, 45)))
                        .addComponent(logoCurso)))
                .addContainerGap(32, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(informacion)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(informacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(search, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(delete, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(update, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(read, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(create, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(logoCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ultimo, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(siguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(anterior, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(primero, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }
    
    //Getters
    public static VistaGraficaCurso getInstancia() {
        if (instancia == null) {
            instancia = new VistaGraficaCurso();
        }
        return instancia;
    }
    
    public JButton getCreate() {
        return create;
    }
    
    public JButton getRead() {
        return read;
    }
    
    public JButton getUpdate() {
        return update;
    }
    
    public JButton getDelete() {
        return delete;
    }
    
    public JButton getSearch() {
        return search;
    }
    
    public JButton getPrimero() {
        return primero;
    }
    
    public JButton getAnterior() {
        return anterior;
    }
    
    public JButton getSiguiente() {
        return siguiente;
    }
    
    public JButton getUltimo() {
        return ultimo;
    }
    
    public JButton getGuardar() {
        return guardar;
    }
    
    public JButton getCancelar() {
        return cancelar;
    }
    
    public JButton getSalir() {
        return salir;
    }
    
    public JLabel getInfo() {
        return informacion;
    }
    
    public JTextField getTid() {
        return tid;
    }
    
    public JTextField getTnombre() {
        return tnombre;
    }
    
    public JTextField getThoras() {
        return thoras;
    }
}