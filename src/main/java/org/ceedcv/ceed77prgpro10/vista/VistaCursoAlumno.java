package org.ceedcv.ceed77prgpro10.vista;

import javax.swing.*;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class VistaCursoAlumno extends JInternalFrame {
    private static VistaCursoAlumno instancia;
    private JButton alumno = new JButton();
    private JButton curso = new JButton();
    private JButton matricula = new JButton();
    private JButton refrescar = new JButton();
    private JButton salir = new JButton();
    private JButton alumno2 = new JButton();
    private JButton curso2 = new JButton();
    private JButton matricula2 = new JButton();
    private JButton refrescar2 = new JButton();
    private JButton salir2 = new JButton();
    private JScrollPane jScrollPane6 = new JScrollPane();
    private JScrollPane jScrollPane5 = new JScrollPane();
    private JScrollPane jScrollPane3 = new JScrollPane();
    private JScrollPane jScrollPane4 = new JScrollPane();
    private JPanel pAlumnos = new JPanel();
    private JPanel pCursos = new JPanel();
    private JPanel panelAluCur = new JPanel();
    private JPanel panelCurAlu = new JPanel();
    private JPanel pAluCur = new JPanel();
    private JPanel pCurAlu = new JPanel();
    private JTable tAlumnos = new JTable();
    private JTable tCursos = new JTable();
    private JTable tAluCur = new JTable();
    private JTable tCurAlu = new JTable();
    private JTabbedPane tabCurAlu = new JTabbedPane();
    
    private VistaCursoAlumno() {
        //Llamamos al constructor de la clase JFrame
        super("Cursos por alumno || Alumnos por curso");
        
        //Configuramos el tamaño, comportamiento del aspa y aspecto general de la GUI
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.err.println("No se ha podido configurar el Look and Feel escogido");
            e.printStackTrace();
        }
        setResizable(false);
        setSize(690,344);
        setResizable(false);       
        setMaximizable(true);
        setIconifiable(true);
        setClosable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        //Configuramos los distintos componentes de la GUI
        //Botones
        alumno.setToolTipText("Abrir el panel de alumnos");
        alumno.setIcon(new ImageIcon(getClass().getResource("/imagenes/alumno.png")));
        curso.setToolTipText("Abrir el panel de cursos");
        curso.setIcon(new ImageIcon(getClass().getResource("/imagenes/curso.png")));
        matricula.setToolTipText("Abrir el panel de matrículas");
        matricula.setIcon(new ImageIcon(getClass().getResource("/imagenes/matricula.png")));
        refrescar.setToolTipText("Refrescar las tablas");
        refrescar.setIcon(new ImageIcon(getClass().getResource("/imagenes/update.png")));
        salir.setToolTipText("Salir de la aplicación");
        salir.setIcon(new ImageIcon(getClass().getResource("/imagenes/salir.png")));
        alumno2.setToolTipText("Abrir el panel de alumnos");
        alumno2.setIcon(new ImageIcon(getClass().getResource("/imagenes/alumno.png")));
        curso2.setToolTipText("Abrir el panel de cursos");
        curso2.setIcon(new ImageIcon(getClass().getResource("/imagenes/curso.png")));
        matricula2.setToolTipText("Abrir el panel de matrículas");
        matricula2.setIcon(new ImageIcon(getClass().getResource("/imagenes/matricula.png")));
        refrescar2.setToolTipText("Refrescar las tablas");
        refrescar2.setIcon(new ImageIcon(getClass().getResource("/imagenes/update.png")));
        salir2.setToolTipText("Salir de la aplicación");
        salir2.setIcon(new ImageIcon(getClass().getResource("/imagenes/salir.png")));
        //Paneles
        pCursos.setBorder(BorderFactory.createTitledBorder("Cursos disponibles"));
        jScrollPane5.setViewportView(tCursos);
        pAlumnos.setBorder(BorderFactory.createTitledBorder("Alumnos matriculados en el curso seleccionado"));
        jScrollPane6.setViewportView(tAlumnos);
        pAluCur.setBorder(BorderFactory.createTitledBorder("Alumnos disponibles"));
        jScrollPane3.setViewportView(tAluCur);
        pCurAlu.setBorder(BorderFactory.createTitledBorder("Cursos en los que está matriculado el alumno"));
        jScrollPane4.setViewportView(tCurAlu);
        tabCurAlu.addTab("Cursos por alumno", panelCurAlu);
        tabCurAlu.addTab("Alumnos por curso", panelAluCur);
       
        javax.swing.GroupLayout pCursosLayout = new javax.swing.GroupLayout(pCursos);
        pCursos.setLayout(pCursosLayout);
        pCursosLayout.setHorizontalGroup(
            pCursosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 658, Short.MAX_VALUE)
        );
        pCursosLayout.setVerticalGroup(
            pCursosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pCursosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))
        );
        javax.swing.GroupLayout pAlumnosLayout = new javax.swing.GroupLayout(pAlumnos);
        pAlumnos.setLayout(pAlumnosLayout);
        pAlumnosLayout.setHorizontalGroup(
            pAlumnosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6)
        );
        pAlumnosLayout.setVerticalGroup(
            pAlumnosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pAlumnosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
        );
        javax.swing.GroupLayout panelAluCurLayout = new javax.swing.GroupLayout(panelAluCur);
        panelAluCur.setLayout(panelAluCurLayout);
        panelAluCurLayout.setHorizontalGroup(
            panelAluCurLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAluCurLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAluCurLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pCursos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pAlumnos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelAluCurLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(alumno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(curso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(matricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(refrescar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(salir, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)))
                .addGap(38, 38, 38))
        );
        panelAluCurLayout.setVerticalGroup(
            panelAluCurLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAluCurLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pCursos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pAlumnos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(panelAluCurLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(matricula, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(alumno, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(curso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refrescar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        javax.swing.GroupLayout pAluCurLayout = new javax.swing.GroupLayout(pAluCur);
        pAluCur.setLayout(pAluCurLayout);
        pAluCurLayout.setHorizontalGroup(
            pAluCurLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 658, Short.MAX_VALUE)
        );
        pAluCurLayout.setVerticalGroup(
            pAluCurLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pAluCurLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))
        );
        javax.swing.GroupLayout pCurAluLayout = new javax.swing.GroupLayout(pCurAlu);
        pCurAlu.setLayout(pCurAluLayout);
        pCurAluLayout.setHorizontalGroup(
            pCurAluLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4)
        );
        pCurAluLayout.setVerticalGroup(
            pCurAluLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pCurAluLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
        );
        javax.swing.GroupLayout panelCurAluLayout = new javax.swing.GroupLayout(panelCurAlu);
        panelCurAlu.setLayout(panelCurAluLayout);
        panelCurAluLayout.setHorizontalGroup(
            panelCurAluLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCurAluLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCurAluLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pAluCur, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pCurAlu, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelCurAluLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(alumno2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(curso2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(matricula2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(refrescar2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(salir2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)))
                .addGap(38, 38, 38))
        );
        panelCurAluLayout.setVerticalGroup(
            panelCurAluLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCurAluLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pAluCur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pCurAlu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(panelCurAluLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(matricula2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(alumno2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(curso2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salir2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(refrescar2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabCurAlu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabCurAlu, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }

    //Getters
    public JButton getAlumno() {
        return alumno;
    }
    
    public JButton getAlumno2() {
        return alumno2;
    }

    public JButton getCurso() {
        return curso;
    }

    public JButton getCurso2() {
        return curso2;
    }

    public JButton getMatricula() {
        return matricula;
    }

    public JButton getMatricula2() {
        return matricula2;
    }

    public JButton getRefrescar() {
        return refrescar;
    }

    public JButton getRefrescar2() {
        return refrescar2;
    }

    public JButton getSalir() {
        return salir;
    }

    public JButton getSalir2() {
        return salir2;
    }

    public JTable gettAlumnos() {
        return tAlumnos;
    }

    public JTable gettAlumnos2() {
        return tAluCur;
    }

    public JTable gettCursos() {
        return tCursos;
    }

    public JTable gettCursos2() {
        return tCurAlu;
    }
    
    public static VistaCursoAlumno getInstancia() {
        if (instancia == null) {
            instancia = new VistaCursoAlumno();
        }
        return instancia;
    }
    
    public JTabbedPane getTabs() {
        return tabCurAlu;
    }
}